# MTG CCU

Donc il s’agit du Cryo Cooler Unit FM01 pour le Flexible Combined Imager d’un Meteosat de 3ème génération
Au boulot, on appelle ça : le CCU du FCI de MTG-I (plus simple non ?)

#Space #Engineering #Cryocooling

 ![](Fig1) 

(merci @kerybas, pour le titre complet que je n’avais jamais vu ^^)

https://aip.scitation.org/doi/pdf/10.1063/1.4860745

## Utilité

Il s’agit d’une unité de cryocooling. Ça produit un froid poussé, ici 50 K (-223 °C). Certaines applications vont plus loin jusqu’à 2 K.

(https://www.researchgate.net/publication/222140567_A_3He_Pulse_Tube_Cooler_Operating_Down_to_13_K)

Les avantages de cette techno sont multiples :
– simple (sur le papier. Le pilotage c’est autre chose)
– Pas de perte de gaz cryogénique (on utilise qu’un volume fini d’hélium pour faire fonctionner la machine tout au long de sa vie)
– Une seule pièce mobile (moins de points de fragilité/défaillance)
– Peu de vibration au niveau du doigt froid (contrairement à un compresseur sterling à deux pistons)
– Très basse température (comme vu au-dessus)

Par contre, c’est de la faible puissance. Il ne faut pas s’attendre à produire des glaçons en quantité (bien qu’on peut en produit et c’est catastrophique dans mon cas !).



Celui-ci produit 5W de froid pour 160W d'électricité !

![](./Fig2.png)

## Les différents éléments

![](./Fig3.jpg)

Il faut un piston (ou alors une bonne sono comme suggérée par @Aviaponcho).
Un « régénateur ». C’est un milieu très poreux à grande chaleur spécifique et bonne conduction. En gros, ça ne doit pas retenir le gaz, ou juste ce qui est nécessaire pour échanger rapidement de la chaleur avec lui. Et il faut qu’il soit capable de stocker de la chaleur (ou un déficit de chaleur) et être homogène en température.
Un tube ouvert à une extrémité et fermé à l’autre avec des échangeurs de chaleur de chaque côté.
Le gaz à l’intérieur c’est de l’hélium (il n’est jamais renouvelé).

Et c’est tout sur le papier. Des variations design existent. Il faut aussi rajouter l'électronique de contrôle, les systèmes d'échange externe, etc.

![](./Fig4.jpg)

## Fonctionnement

Il s'agit d'un cycle à 4 temps.

![](./Fig5.jpg)

1. Le piston comprime le gaz :
Dans le tube, le gaz est comprimé vers l’extrémité fermée (Qh).
Le gaz est comprimé contre la paroi de façon adiabatique (pas d’échange de chaleur avec l’extérieur). Il y a donc un gradient de pression entre l’entrée et la fin (basse pression près de l’entrée, haute pression près de la fin du tube). Cela se traduit par un gradient de température dans le tube.

2. L’extrémité fermée (Qh) du tube absorbe la chaleur pour avoir un équilibre de température entre le gaz et la paroi. Un échangeur en contact avec le gaz peut être utilisé pour améliorer le process.
La température du gaz comprimé chute pour être à l’équilibre.

3. Le piston se rétracte
Dans le tube, le gaz subit une détente adiabatique. La température chute encore plus.

4. Le gaz absorbe de la chaleur du tube. On se retrouve avec une partie chaud (Qh) et une partie froide (Qc). Les deux ne se mélangent pas surtout si on a une onde acoustique (donc haute fréquence).

Si on n’en fait rien, le processus finit par se stabiliser avec un gradient de température dans le tube. On arrive plus à extraire d’énergie. C’est là on rentre en jeu le régénérateur.

1. Au moment de la compression, du gaz est chassé du régénérateur vers le tube au niveau de l’extrémité ouverte.
Ce gaz, à température du régénérateur, va être refroidi en entrant en contact avec l’échangeur au niveau de l’extrémité ouverte (Qc). En sortant du tube, et en allant de nouveau dans le régénérateur, il va absorber la chaleur de celui-ci.

En évacuant la chaleur au niveau de la partie chaude (Qh) et en mettant en contact avec les équipements à refroidir le doigt froid (Qc) on peut donc extraire de chaleur indéfiniment. (Pour les aficionados, on extrait de la chaleur à rebours d’un gradient de température)



http://large.stanford.edu/courses/2007/ph210/bert2/

Bonus : C'est le même principe pour refroidir l'instrument MIRI sur JWST (pas le même modèle, il est biennnn plus gros)

https://jwst.nasa.gov/content/about/innovations/cryocooler.html



![](./Fig6.jpg)
