import collections
import os
import pickle
import re
import zipfile
from os import listdir, path, walk
from os.path import isfile, join
from pathlib import Path
from typing import List, Optional, Union


def exists(file: str) -> bool:
    return path.exists(file)


def createFolder(folder: str) -> None:
    os.makedirs(folder, exist_ok=True)


def listFiles(folderPath: str, recursive: bool = False, extension: List[str] = None) -> List[str]:
    """Provide the list of file contained in a folder.
    
    Parameters
    ----------
    folderPath: str 
        Folder where the search shall start
    recursive: bool, optional
        boolean indicating if the search shall include subfolder
    extension: List[str], str, optional
        file with the specific extentions to keep (does not include the point of the extension (for instance, text file are defined as 'txt'
    
    Returns
    ----------
    files: List[str]
        List of files with their complet path
    """

    # return [f for f in listdir(folderPath) if isfile(join(folderPath, f))]

    files = []
    for (dirpath, dirnames, filenames) in walk(top=folderPath):
        files.extend([dirpath + os.sep + filename for filename in filenames])
        if not recursive:
            break

    if extension is not None:

        # Turn extension to str
        if isinstance(extension, collections.Iterable) and not isinstance(extension, str):
            extension = "$|".join(extension) + "$"
            files = [f for f in files if re.search(extension, f)]
        else:
            files = [f for f in files if f.endswith(extension)]

    return files


def getBasename(filepath: str):
    """Get the filename of a file pointed by the provided filepath
    
    Parameters
    ----------
    filepath: str
        Filepath from which the filename shall be extracted
        
    Return
    ------
    Return the filename"""

    return path.basename(filepath)


def getFilename(filepath: str) -> str:
    """
    Get the name file with no path and no extension
    
    Parameters
    ----------
    filepath: str
        Filepath from which the filename shall be extracted
        
    Return
    ------
    Return the file name
    """

    return os.path.splitext(getBasename(filepath))[0]


def getFileExtension(filepath: str):
    """Get the extension of the file
    
    Parameters
    ----------
    filepath: str
        Filepath from the extension is to be determined
        
    Return
    ------
    Return the filename and the extension"""

    name = getBasename(filepath)
    return os.path.splitext(name)


def getParent(filename: str) -> Path:
    """Get the parent directory path of a file
    
    Parameter
    ---------
    filename: str
        Filename of the file
        
    Return
    ------
    parent folder path"""

    path = Path(filename)
    return path.parent


def getParentString(filename: str) -> str:
    """Get the parent directory path of a file as a string path
    
    Parameter
    ---------
    filename: str
        Filename of the file
        
    Return
    ------
    parent folder path as a string"""

    return os.sep.join(getParent(filename).parts)


## Access to file
def readFile(filepath: str, verbose: bool = True) -> str:

    f = open(filepath, "r")
    toReturn = f.read()
    f.close()

    if verbose:
        print("File " + filepath + " succefully wrote.")

    return toReturn


def writeFile(filepath: str, fileContent: str, verbose: bool = True):

    f = open(filepath, "w")
    f.write(fileContent)
    f.close()

    if verbose:
        print("File " + filepath + " succefully wrote.")


def deleteFile(filepath: Union[str, List[str]], verbose: bool = True):

    if isinstance(filepath, str):
        os.remove(filepath)
        if verbose:
            print("File " + filepath + " succefully deleted.")
    elif isinstance(filepath, collections.Iterable):
        for f in filepath:
            deleteFile(f, verbose=verbose)
    else:
        if verbose:
            print("No file to delete for: " + str(filepath))


## Zip files
def uncompressFile(
    filepath: Union[str, List[str]], folderTarget: Optional[str] = None, verbose: bool = True
):
    """Uncompress a zip file or a list of zip file
    
    Parameters
    ----------
    filepath: str, List[str]
        Filepath of the zip file to unzip
    folderTarget: str, optional
        Folder where to unzip the file. If none, then unzip in the same folder than the zip file"""

    # If unique file
    if isinstance(filepath, str):
        filepath = [filepath]  # Turn it a as a list

    for f in filepath:
        if not zipfile.is_zipfile(f):
            if verbose:
                print(f + " is not a zip file.")
            pass

        if not folderTarget:
            folderTarget = getParent(f)

        with zipfile.ZipFile(f, "r") as zip_ref:
            zip_ref.extractall(folderTarget)

        if verbose:
            print("File " + str(f) + " unzipped in " + str(folderTarget))


# Store object
def parse(data, filename: str):
    """Parse an object to the file defined by the filename."""

    with open(filename, "wb") as file:
        pickle.dump(data, file)
    # with open(filename, "w") as file:
    #     frozen = jsonpickle.encode(data, indent=4)
    #     print(frozen, file=file)


def unparse(filename: str) -> object:
    """Unparse an object from a file defined by the filename."""

    with open(filename, "rb") as file:
        data = pickle.load(file)
        # data = jsonpickle.decode(file.read())
        return data

    return None
