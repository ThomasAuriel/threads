from textwrap import wrap

import matplotlib
import numpy as np
import scipy.stats as stats
from matplotlib import pyplot as plt

from data import units
from lib.pandas import PandasOperation, PandasSelection

matplotlib.use("agg")


def createTable(*datas, index):

    columns = []
    rowLabels = []
    rowValues = []

    for data in datas:

        unit = units.getUnit(data)
        dataRowLabels = [
            "Count\nNb points",
            "Mean\n{}".format(unit.symbol),
            "Std\n{}".format(unit.symbol),
            "Minimum\n{}".format(unit.symbol),
            "Maximum\n{}".format(unit.symbol),
            "25%\n{}".format(unit.symbol),
            "50%\n{}".format(unit.symbol),
            "75%\n{}".format(unit.symbol),
        ]

        dataRowValues = []
        columns = []

        for column in data.index.unique(level=index):

            tmp = PandasSelection.select(data, index, column)
            tmpStats = PandasOperation.stats(tmp)

            dataRowValues.append(
                [
                    "{0:.0f}".format(tmpStats.iloc[:, 0]["count"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["mean"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["std"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["min"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["max"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["25%"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["50%"]),
                    "{0:.2f}".format(tmpStats.iloc[:, 0]["75%"]),
                ]
            )

            columns.append(column)

        dataRowLabels = np.array(dataRowLabels)
        dataRowValues = np.array(dataRowValues)

        rowLabels.append(dataRowLabels)
        rowValues.append(dataRowValues)

    # Concatenate
    rowLabels = np.hstack(rowLabels)
    rowValues = np.hstack(rowValues)

    # Transpose the matrix
    rowLabels = np.transpose(rowLabels)
    rowValues = np.transpose(rowValues)

    table = addTable(rowValues, columns, rowLabels, width=1, height=1)

    return table


def createRegretionTable(data0, data1, index):

    columns = []
    rowLabels = []
    rowValues = []

    unit0 = units.getUnit(data0)
    unit1 = units.getUnit(data1)
    dataRowLabels = [
        "slope\n{}/{}".format(unit0.symbol, unit1.symbol),
        "intercept",
        "r_value",
        "p_value",
        "std_err",
    ]

    dataRowValues = []
    columns = []

    for column in data0.index.unique(level=index):

        data0_tmp = PandasSelection.select(data0, index, column)
        data1_tmp = PandasSelection.select(data1, index, column)

        slope, intercept, r_value, p_value, std_err = stats.linregress(
            np.transpose(data0_tmp.values)[0], np.transpose(data1_tmp.values)[0]
        )
        dataRowValues.append(
            [
                "{0:.2f}".format(slope),
                "{0:.2f}".format(intercept),
                "{0:.2f}".format(r_value),
                "{0:.2f}".format(p_value),
                "{0:.2f}".format(std_err),
            ]
        )

        columns.append(column)

    dataRowLabels = np.array(dataRowLabels)
    dataRowValues = np.array(dataRowValues)

    rowLabels.append(dataRowLabels)
    rowValues.append(dataRowValues)

    # Concatenate
    rowLabels = np.hstack(rowLabels)
    rowValues = np.hstack(rowValues)

    # Transpose the matrix
    rowLabels = np.transpose(rowLabels)
    rowValues = np.transpose(rowValues)

    table = addTable(rowValues, columns, rowLabels, width=1, height=1)

    return table


def addTable(cell_text, columns, rows, width=1, height=1):

    columns = ["\n".join(wrap(str(text), 15)) for text in columns]

    width = len(columns) * 0.15 * width
    height = len(rows) * 0.09 * height
    table = plt.table(
        cellText=cell_text,
        cellLoc="center",
        rowLabels=rows,
        rowLoc="center",
        colLabels=columns,
        colLoc="center",
        loc="upper center",
        bbox=[0.5 - width / 2, -0.15 - height, width, height],
    )
    table.auto_set_font_size(False)

    return table
