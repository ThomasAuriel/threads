import random

import matplotlib
from cycler import cycler

# https://matplotlib.org/3.1.0/gallery/color/named_colors.html

colors = {
    # Countries
    "AL": "#48bd6e",
    "AT": "#3366ff",
    "BA": "#1e0059",
    "BE": "#c68c53",
    "BG": "#017d03",
    "BY": "#be9b7b",
    "CH": "#ff3300",
    "CZ": "#33ccff",
    "DE": "#795548",
    "DE-LU": "#795548",
    "DK": "#00cca3",
    "DK-1": "#00cca3",
    "DK-2": "#00cca3",
    "EE": "#9bc2db",
    "ES": "#f4d03f",
    "FI": "#96cc60",
    "FR": "#0066ff",
    "GB": "#0099cc",
    "GR": "#05464f",
    "HR": "#38a6dd",
    "HU": "#ea6112",
    "IE": "#b1ccad",
    "IE-SEM": "#b1ccad",
    "IT": "#00cc00",
    "IT-NORD": "#00cc00",
    "IT-CNOR": "#00cc00",
    "IT-CSUD": "#00cc00",
    "IT-SUD": "#00cc00",
    "IT-SARD": "#00cc00",
    "IT-SICI": "#00cc00",
    "LT": "#dbc5bb",
    "LU": "#9966ff",
    "LV": "#198fc8",
    "ME": "#f19526",
    "MK": "#7f5991",
    "MT": "#d37cb1",
    "NL": "#ff3399",
    "NO": "#cc0066",
    "NO-1": "#cc0066",
    "NO-2": "#cc0066",
    "NO-3": "#cc0066",
    "NO-4": "#cc0066",
    "NO-5": "#cc0066",
    "PL": "#c3c3c3",
    "PT": "#ff5733",
    "RO": "#76c6be",
    "RS": "#a77087",
    "RU": "#e04322",
    "RU-KGD": "#c73715",
    "SE": "#ffc34d",
    "SE-1": "#ffc34d",
    "SE-2": "#ffc34d",
    "SE-3": "#ffc34d",
    "SE-4": "#ffc34d",
    "SI": "#729872",
    "SK": "#591a29",
    "TR": "#a8649e",
    "UA": "#fb0754",
    # IPCC
    "Biomass": "#1e8449",
    "Lignite": "k",
    "Coal": "#4e342e",
    "Gas": "tab:brown",
    "Oil": "tab:gray",
    "Waste": "#3d8071",
    "Other": "darkgray",
    "Nuclear": "tab:olive",
    "Other renewable": "#d3d3d3",
    "Geothermal": "tab:red",
    "Hydro": "tab:purple",
    "Wind Offshore": "tab:blue",
    "Wind Onshore": "tab:cyan",
    "Solar": "gold",
    # ENTSOE
    "Biomass": "#1e8449",
    "Fossil Brown coal/Lignite": "k",
    "Fossil Hard coal": "#4e342e",
    "Fossil Peat": "#81554b",
    "Fossil Coal-derived gas": "#81734b",
    "Fossil Gas": "tab:brown",
    "Fossil Oil": "tab:gray",
    "Fossil Oil shale": "#666666",
    "Waste": "#3d8071",
    "Other": "darkgray",
    "Nuclear": "tab:olive",
    "Other renewable": "#d3d3d3",
    "Geothermal": "tab:red",
    "Hydro Pumped Storage": "#66a3ff",
    "Hydro Run-of-river and poundage": "#4d94ff",
    "Hydro Water Reservoir": "#4d94ff",
    "Marine": "#1a1aff",
    "Wind Offshore": "tab:blue",
    "Wind Onshore": "tab:cyan",
    "wind": "tab:blue",
    "Solar": "gold",
    "solar": "gold",
    # Other categories
    "Import": "tab:blue",
    "Export": "tab:orange",
    "Load": "tab:blue",
    # Mixes
    "Fossil": "#303030",
    "Renewable": "#1e8449",
    "Hydro": "tab:purple",
    "Nuclear": "tab:olive",
    # Other
    "Other": "darkgray",
    0: "tab:blue",
    "CO2 Intensity": "tab:blue",
}


def defineColorCycle(categories):

    colorCycle = [
        colors[category]
        if category in colors
        else "#" + "".join([random.choice("0123456789ABCDEF") for index in range(6)])
        for category in categories
    ]
    matplotlib.rcParams["axes.prop_cycle"] = cycler(color=colorCycle)

    return colorCycle
