from typing import List, Union

from pandas import DataFrame, Grouper

from data import index, units


def sumOver(data: DataFrame, levels: Union[List[str], List[int], str, int]) -> DataFrame:
    """
    Sum over the selected levels
    
    data:
        data to sum
    levels:
        levels to sum over
    """

    result = data.sum(level=levels)
    return result


def sumOverTime(data: DataFrame, level: str = index.datetime, freq: str = "D") -> DataFrame:

    level_values = data.index.get_level_values
    return data.groupby([level_values(i) for i in [0, 1]] + [Grouper(freq=freq, level=level)]).sum()


def meanOver(data, levels):
    """
    Mean over the selected levels
    
    data:
        data to sum
    levels:
        levels to sum over
    """

    result = data.mean(level=levels)
    return result


def ratio(data1: Union[List, None] = None, data2: Union[List, None] = None, levels: List = []):
    """
    Ratio of data1 over data2
    
    data1, data2:
        data used to compute ratio
    levels:
        levels to sum over
    """

    if data2 is None:
        data2 = data1

    result = data1.div(data2.groupby(level=levels).max(), fill_value=0).fillna(0)
    result = units.setUnit(data=result, unit=units.Ratio)

    return result


def resampleOverTime(data: DataFrame, level: str = index.datetime, freq: str = "D") -> DataFrame:

    level_values = data.index.get_level_values
    return data.groupby(
        [level_values(i) for i in [0, 1]] + [Grouper(freq=freq, level=level)]
    ).mean()


def stats(data):

    return data.describe()
