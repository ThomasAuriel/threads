import os
from typing import List, Tuple, Union

import numpy
import statsmodels.api as sm
import statsmodels.formula.api as smf
from matplotlib import pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon
from pandas import DataFrame, Series
from scipy import stats

from utils.file import FileUtils
from utils.matplotlib import FigureUtils
from utils.pandas import PandasSelection


## Plots
def plot(
    filename: str,
    values: DataFrame,
    labels: Union[List[str], None] = None,
    title: str = "Untitled",
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    ax.plot(
        values, antialiased=True, linestyle="-", marker="", linewidth=1,
    )

    # Add legend
    lgd = FigureUtils.addLegend(ax)
    artists.append(lgd)

    # Title
    plt.title(title)
    if labels is not None:
        plt.xticks(range(0, len(labels)), labels, rotation=90)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotDate(
    filename: str,
    values: DataFrame,
    labels: Union[List[str], None] = None,
    title: str = "Untitled",
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    categories = list(values.columns)
    values = values.reset_index()
    for idx, category in enumerate(categories):

        x = values["date"].values.flatten()
        y = values[category].values.flatten()

        ax.plot_date(
            x=x, y=y, label=category, antialiased=True, linestyle="-", marker="", linewidth=1,
        )

    # Add legend
    lgd = FigureUtils.addLegend(ax)
    artists.append(lgd)

    # Title
    plt.title(title)
    if labels is not None:
        plt.xticks(range(0, len(labels)), labels, rotation=90)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotScatter(
    filename: str,
    x: DataFrame,
    y: DataFrame,
    xlim: Union[None, Tuple[int, int]],
    ylim: Union[None, Tuple[int, int]],
    xlabel: str,
    ylabel: str,
    polygon: Union[None, List[List[float]]],
    title: str = "Untitled",
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    if polygon:
        polygon = Polygon(polygon, True)
        polygon.set_color("#7f7f7f")
        patches = [polygon]

        p = PatchCollection(patches, alpha=0.25)
        p.set_color("#7f7f7f")
        # p.set_array(numpy.array(colors))
        ax.add_collection(p)

    # then data
    plt.scatter(
        x.values, y.values, marker=".",
    )

    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    # Title
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotHistogram(
    filename: str,
    values: DataFrame,
    title: str = "Untitled",
    bins: int = 50,
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    plt.hist(values, bins=bins)

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    # Title
    plt.title(title)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotHistogramTwoAxes(
    filename: str,
    values: Tuple[DataFrame, DataFrame],
    title: str = "Untitled",
    xlabel: Tuple[str, str] = ("value", "z-score"),
    bins: int = 50,
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    plt.hist(values[0], bins=bins)
    ax.set_xlabel(xlabel[0])

    # Create the second figure
    ax2 = ax.twiny()

    # Plot
    plt.hist(values[1], bins=bins)  # z-score
    ax2.set_xlabel(xlabel[1])

    # From : https://stackoverflow.com/a/31808931

    # Move twinned axis ticks and label from top to bottom
    ax2.xaxis.set_ticks_position("bottom")
    ax2.xaxis.set_label_position("bottom")
    # Offset the twin axis below the host
    ax2.spines["bottom"].set_position(("axes", -0.4))
    # Turn on the frame for the twin axis, but then hide all
    # but the bottom spine
    ax2.set_frame_on(True)
    ax2.patch.set_visible(False)

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    # Title
    plt.title(title)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotNormalDistribution(
    filename: str,
    title: str,
    data: DataFrame,
    fig=None,
    ax1=None,
    ax2=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax1 == None or artists == None:
        # Create the figure
        fig, ax1, artists = FigureUtils.getFigure(size, 211)

    # Plot
    ax1.hist(data, bins=50, density=True, histtype="stepfilled", label="Data distribution")
    x_norm = numpy.linspace(-5, 5, 50)
    y_norm = stats.norm.pdf(x_norm)
    ax1.plot(x_norm, y_norm, c="#7f7f7f", label="Normal distribution")

    # Add legend
    # lgd = FigureUtils.addLegend(ax1)
    # artists.append(lgd)
    ax1.title.set_text(title + "\nNormal distribution test")

    # Subplot 2
    if ax2 == None:
        # Create the ax2
        ax2 = FigureUtils.addSubplot(212)

    # Plot
    ax2.hist(
        data,
        bins=50,
        density=True,
        histtype="stepfilled",
        cumulative=True,
        label="Cumulative data distribution",
    )
    x_norm = numpy.linspace(-5, 5, 50)
    y_norm = stats.norm.cdf(x_norm)
    ax2.plot(x_norm, y_norm, c="#7f7f7f", label="Cumulative normal distribution")

    # Add legend
    # lgd = FigureUtils.addLegend(ax2)
    # artists.append(lgd)
    ax2.title.set_text("Cumulative normal distribution test")

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax1, ax2, artists


def plotNormalProbability(
    filename: str,
    title: str,
    data: DataFrame,
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    # pp = sm.ProbPlot(data)
    qq = sm.qqplot(
        data,
        marker=".",
        markerfacecolor="#1f77b4",
        markeredgecolor="#1f77b4",
        label="Normal probability test",
        ax=ax,
    )
    qq = sm.qqline(line="45", fmt="#7f7f7f", ax=ax)

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    plt.title(title)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotAutoCorrelationResidus(
    filename: str,
    title: str,
    data: DataFrame,
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    ax.acorr(data, usevlines=True, maxlags=50, normed=True, lw=2)

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    plt.title(title + "\Auto correlation")

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotCrossCorrelation(
    filename: str,
    title: str,
    x: DataFrame,
    y: DataFrame,
    xlabel: str = Union[str, None],
    ylabel: str = Union[str, None],
    displaySigma: bool = True,
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    # ax.xcorr(x, y, usevlines=True, maxlags=50, normed=True, lw=2)
    ax.scatter(
        x,
        y,
        marker=".",
        # color=colorCycle[idx],
        alpha=0.25,
        zorder=2,
    )

    # Correlation

    n = 2
    idx = numpy.isfinite(x) & numpy.isfinite(y)
    results = smf.ols("y ~ x + I(x**2)", data={"x": x, "y": y}).fit()
    x_model = x[idx].sort_values()
    y_model = results.predict({"x": x_model})
    ax.plot(x_model, y_model, c="#7f7f7f")

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    plt.title(
        title
        # + "\nRegression equation: y = {:.2f} + {:.2f} x + {:.2f} x^2".format(*results.params.values)
        # + "\nRegression equation p-values: [{:.2f}, {:.2f}, {:.2f}]".format(*results.pvalues.values)
    )
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists


def plotInfluence(
    filename: str,
    result: DataFrame,
    title: str = "Untitled",
    fig=None,
    ax=None,
    artists=None,
    dpi: int = 360,
    size: Tuple[int, int] = (10, 6),
):

    if fig == None or ax == None or artists == None:
        # Create the figure
        fig, ax, artists = FigureUtils.getFigure(size, 111)

    # Plot
    sm.graphics.influence_plot(result, criterion="cooks", ax=ax)

    # Add legend
    # lgd = FigureUtils.addLegend(ax)
    # artists.append(lgd)
    # Title
    plt.title(title)

    """ Save the plot """
    parent = FileUtils.getParent(filename)
    FileUtils.createFolder(folder=parent)

    plt.tight_layout()
    plt.savefig(filename, dpi=dpi, bbox_extra_artists=(*artists,))
    # plt.close("all")

    return fig, ax, artists
