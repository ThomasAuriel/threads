import json
import os

import pandas
import requests

from utils.file import FileUtils


def getData(url: str, filepath: str, replaceIfExist: bool = False, verbose: bool = True):
    """
    Get data from IEA
    """

    if replaceIfExist or not os.path.exists(filepath):

        if verbose:
            print("Collect data")
        resp = requests.get(url=url)
        data = resp.json()
        data = json.dumps(data, indent=4, sort_keys=True)  # Get the data

        if verbose:
            print("Save data : " + filepath)
        parent = FileUtils.getParent(filepath)
        FileUtils.createFolder(folder=parent)
        FileUtils.writeFile(filepath=filepath, fileContent=data)
    else:
        if verbose:
            print("Do not replace existing file : " + filepath)


if __name__ == "__main__":

    # Download all available series
    url = "https://ewoken.github.io/world-data-app/data/statistics.json"
    filepath = "./data/IEA/series.json"
    getData(url=url, filepath=filepath)

    # Population
    url = "https://ewoken.github.io/world-data-app/data/POPULATION/all.json"
    filepath = "./data/IEA/POPULATION.json"
    getData(url=url, filepath=filepath)

    # Consumption
    url = "https://ewoken.github.io/world-data-app/data/PRIMARY_ENERGY_CONSUMPTION_MTOE/all.json"
    filepath = "./data/IEA/PRIMARY_ENERGY_CONSUMPTION_MTOE.json"
    getData(url=url, filepath=filepath)

    # Production
    url = "https://ewoken.github.io/world-data-app/data/GDP_2010_USD/all.json"
    filepath = "./data/IEA/GDP_2010_USD.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/PRIMARY_ENERGY_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/PRIMARY_ENERGY_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/WIND_CAPACITY_GW/all.json"
    filepath = "./data/IEA/WIND_CAPACITY_GW.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/SOLAR_CAPACITY_GW/all.json"
    filepath = "./data/IEA/SOLAR_CAPACITY_GW.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)

    # Nuclear
    url = "https://ewoken.github.io/world-data-app/data/NUCLEAR_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/NUCLEAR_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/NUCLEAR_CAPACITY_GW/all.json"
    filepath = "./data/IEA/NUCLEAR_CAPACITY_GW.json"
    getData(url=url, filepath=filepath)

    # Renewable
    url = "https://ewoken.github.io/world-data-app/data/WIND_GENERATION_TWH/all.json"
    filepath = "./data/IEA/WIND_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/WIND_CAPACITY_GW/all.json"
    filepath = "./data/IEA/WIND_CAPACITY_GW.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/WIND_GENERATION_TWH/all.json"
    filepath = "./data/IEA/SOLAR_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/SOLAR_CAPACITY_GW/all.json"
    filepath = "./data/IEA/SOLAR_CAPACITY_GW.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/GEOTH_SOLAR_WIND_TIDE_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/GEOTH_SOLAR_WIND_TIDE_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)

    url = "https://ewoken.github.io/world-data-app/data/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)

    # Hydro
    url = "https://ewoken.github.io/world-data-app/data/HYDRO_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/HYDRO_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)

    # Fossil
    url = "https://ewoken.github.io/world-data-app/data/COAL_CONSUMPTION_MTOE/all.json"
    filepath = "./data/IEA/COAL_CONSUMPTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/COAL_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/COAL_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/GAS_CONSUMPTION_MTOE/all.json"
    filepath = "./data/IEA/GAS_CONSUMPTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/GAS_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/GAS_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/OIL_CONSUMPTION_MTOE/all.json"
    filepath = "./data/IEA/OIL_CONSUMPTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/OIL_PRODUCTION_MTOE/all.json"
    filepath = "./data/IEA/OIL_PRODUCTION_MTOE.json"
    getData(url=url, filepath=filepath)
    url = "https://ewoken.github.io/world-data-app/data/FOSSIL_FUELS_CAPACITY_GW/all.json"
    filepath = "./data/IEA/FOSSIL_FUELS_CAPACITY_GW.json"
    getData(url=url, filepath=filepath)

    # Electricity generation
    ## All
    url = "https://ewoken.github.io/world-data-app/data/ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Oil
    url = "https://ewoken.github.io/world-data-app/data/OIL_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/OIL_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Gas
    url = "https://ewoken.github.io/world-data-app/data/GAS_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/GAS_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Coal
    url = "https://ewoken.github.io/world-data-app/data/COAL_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/COAL_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Biomass
    url = "https://ewoken.github.io/world-data-app/data/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## EnR
    url = "https://ewoken.github.io/world-data-app/data/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)