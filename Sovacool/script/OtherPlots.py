import json
import os
import sys
from _collections_abc import Iterable
from datetime import datetime
from functools import reduce
from typing import List, Tuple, Union

import matplotlib.pyplot as plt
import numpy
import pandas
import requests
import statsmodels.api as sm
import statsmodels.formula.api as smf
import statsmodels.stats.api as sms
from matplotlib import pyplot as plt
from pandas import DataFrame, Series
from scipy import stats
from statsmodels.compat import lzip

import Plots
from utils import Utils
from utils.file import FileUtils
from utils.matplotlib import FigureUtils
from utils.pandas import PandasIndexes, PandasSelection

# Just to display maximum data in the terminal
# numpy.set_printoptions(threshold=sys.maxsize)
# pandas.set_option("display.max_rows", None, "display.max_columns", None)


def getData(
    countryGroup: List[str] = ["WORLD"],
    start=pandas.Timestamp(datetime(1970, 1, 1), tz="UTC"),
    end=pandas.Timestamp(datetime(2020, 1, 1), tz="Europe/Brussels"),
):

    dfs = []

    # Population
    df_Pop = Utils.getData("./data/IEA/POPULATION.json", SovacoolData=False)
    df_Pop = PandasSelection.selectCountry(df_Pop, countryGroup)
    df_Pop = PandasSelection.selectBetweenDatetime(df_Pop, start, end)
    # df_Pop = df_Pop.rename(columns={"value": "pop"})
    # dfs.append(df_Pop)

    # Nuclear capacity
    df_nucl = Utils.getData("./data/IEA/NUCLEAR_CAPACITY_GW.json", SovacoolData=False)
    df_nucl = PandasSelection.selectCountry(df_nucl, countryGroup)
    df_nucl = PandasSelection.selectBetweenDatetime(df_nucl, start, end)
    df_nucl = df_nucl / df_Pop
    df_nucl = df_nucl.rename(columns={"value": "Nuclear capacity per capita [kW/cap]"})
    dfs.append(df_nucl)

    # Wind capacity
    df_wind = Utils.getData("./data/IEA/WIND_CAPACITY_GW.json", SovacoolData=False)
    df_wind = PandasSelection.selectCountry(df_wind, countryGroup)
    df_wind = PandasSelection.selectBetweenDatetime(df_wind, start, end)
    df_wind = df_wind / df_Pop
    df_wind = df_wind.rename(columns={"value": "Wind capacity per capita [kW/cap]"})
    dfs.append(df_wind)

    # Solar capacity
    df_sola = Utils.getData("./data/IEA/SOLAR_CAPACITY_GW.json", SovacoolData=False)
    df_sola = PandasSelection.selectCountry(df_sola, countryGroup)
    df_sola = PandasSelection.selectBetweenDatetime(df_sola, start, end)
    df_sola = df_sola / df_Pop
    df_sola = df_sola.rename(columns={"value": "Solar capacity per capita [kW/cap]"})
    dfs.append(df_sola)

    # Merge
    data = reduce(
        lambda left, right: pandas.merge(left, right, on=["country", "date"], how="outer"), dfs
    )

    return data


if __name__ == "__main__":

    data = getData()

    Plots.plot(
        filename="./results/OtherPlots/Capacities.png", values=data, title="Capacities over time"
    )
