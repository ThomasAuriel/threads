## Installation

```bash
# For Fedora
sudo dnf install lapack-devel

python -m pip install xlrd --user
python -m pip install statsmodels --user
```
