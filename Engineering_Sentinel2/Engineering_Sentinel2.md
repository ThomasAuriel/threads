[Thread Ingénierie] Il n’y a pas eu beaucoup de réponses détaillées ^^

https://twitter.com/Thomas_Auriel/status/1410596968431194112

Ces trois avions ne sont qu’un seul et unique avion, mais du fait de la géométrie du capteur, les trois bandes de couleurs ne sont pas alignées.

Petite explication.

## Sentinel 2

Le satellite qui a pris la photo est Sentinel-2 (je ne sais pas si c’est 2A ou 2B).

![](./S2-2021_Auto52.jpeg)

https://youtu.be/1OtYCcYYOFw

Il transporte l’instrument MultiSpectral Instrument (MSI). Ce MSI est constitué de miroirs pour focaliser la lumière et de barrettes de pixels sensibles à des longueurs spécifiques.

### Description du MSI

Voici une représentation de l’instrument et de la configuration géométrique des miroirs. La lumière est partagée en deux plans focaux par un prisme. Ce prisme permettant de sélectionner des longueurs d’onde spécifiques.

![](./S2-2021_Auto13.jpeg)

![](./S2-2021_Auto11.jpeg)

Cette séparation est nécessaire du faite des capteurs sur les deux plans focaux qui ne sont pas sensibles aux mêmes fréquences et surtout de technologies différentes pour assurer cette sensibilité. Le premier des plans focaux porte les canaux VNIR (visible et proche infrarouge — Visible and Near Infrared) et le second porte les canaux SWIR (Infrarouge ondes courte — Short waves Infrared).



La répartition des capteurs sur les deux plans focaux est faite de la manière suivante.

(Sur SWIR, il y a seulement 3 par capteur et non pas 10)

![](./S2-2021_Auto17.jpeg)

![](./SUH-WIKI-MSI-300_Instrument_Description_img3.PNG)

La sensibilité des longueurs d’onde se répartit ainsi : 

![](./S2-2021_Auto16.jpeg)

![](./LongueureDOndeCapteur.png)

Je ne sais pas exactement quel satellite a été utilisé entre le 2A et 2B pour cette photo. Mais ce qu’il faut retenir, c’est que les bandes B4, B3 et B2 (rouge, vert, bleu) ont été probablement utilisées.

### Fonctionnement du MSI

Le satellite fonctionne ainsi : il orbite autour de la Terre à une vitesse donnée. Grâce à son télescope, il collecte de la lumière et l’amène sur un capteur. L’image formée n’est pas celle que l’on attend pour une caméra, mais plutôt celle d’une photocopieuse. Le satellite effectue une fauche continue de 32 minutes max (jusqu’à remplir la mémoire de l’instrument) et de 290 km de large (avec une résolution pouvant descendre à 10 m suivant le capteur).

![](./S2-2021_Auto45.jpeg)

https://twitter.com/physicsJ/status/1408765885016248320

L’image se déroule donc sur le capteur en continu. Mais c’est là où ça devient trompeur. Chaque bande de capteurs ne regarde pas exactement dans la même direction du fait de leur répartition et de la géométrie du télescope. En conséquence, si la bande bleue regarde un point, la bande verte regarde un point légèrement en retrait et la bande rouge un point plus en arrière encore.

Pour observer des objets au sol, cela ne pose pas de problème : il suffit de repositionner l’image en connaissant cette parallaxe. Toutefois, un avion peut être à haute altitude. Et l’algorithme se concentre sur le sol. L’avion sera vu avec des projections fausses sur le sol d’où un décalage.
À cela, il faut ajouter le déplacement de l’avion qui va, suivant la configuration sol/avion/satellite, allonger la trace au sol.
![](./1*h9iBdBZSela6GMvJ-TQ_Hw.png)

À cela, il faut ajouter le déplacement de l’avion qui va, suivant la configuration sol/avion/satellite, allonger la trace au sol.

## DSCOVR — D’autres configurations de capteur

Il existe une autre configuration pour cet effet. Le satellite DSCOVR en est un exemple.

![](./DSCOVRInstruments.png)

Le satellite Deep Space Climate Observatory (DSCOVR) est un satellite d’imagerie de la Terre qui est positionné en L1. Pour faire simple, il est entre la Terre et le Soleil de façon constante.

![](./L1_EPIC.png)

Grâce à son instrument Earth Polychromatic Imaging Camera (EPIC), il est capable de prendre une photo de l’hémisphère terrestre en permanence au soleil.

![](./EPICInstrument.png)



Voici le type de photo qu’il est capable de générer.

https://twitter.com/dscovr_epic/status/1410680166221811717

La particularité de cet instrument par rapport à celui de Sentinel-2, c’est qu’il ne s’agit pas d’une barrette, mais d’une matrice.

Sur Sentinel-2, chaque canal est capable d’isoler un ensemble de fréquence. Ici pour le CDD ne mesure que la fréquence souhaitée, une roue portant plusieurs filtres est montée.

![](./EPICWheel.png)

La fréquence de la lumière à photographier est donc sélectionnée en positionnant le filtre voulu devant le CCD. Faire un changement de filtre prend un peu de temps. Ce n’est pas gênant pour ce satellite qui se concentre sur la photographie des nuages et poussières.
Par contre, il arrive qu’un objet passe dans son champ de vision et que cet objet soit suffisamment rapide pour qu’un effet de parallaxe soit visible. Cet objet, c’est la Lune.

![](./Moon1.jpeg)

![](./moon2.gif)

Voilà. Maintenant, vous êtes des spécialistes de la parallaxe dans les images d'observation de la Terre :)
