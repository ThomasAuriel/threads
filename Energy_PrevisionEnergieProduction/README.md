[Thread] Il y a eu un thread intéressant de @Prof_D_sciences (cf https://twitter.com/Prof_D_sciences/status/1296525599280857090).

Prof se demandait : peut-on prédire la production des éoliennes et si cette prévision est fiable. Comme d’habitude, la réponse est plus complexe qu’un simple oui ou non et il l’a bien montré (lisez son thread !).

Je vous propose d’aller un peu plus loin et de regarder à l’échelle de l’Europe. Je vous préviens dès maintenant : le thread risque d’être un peu technique. Je vais également reprendre la structure du thread de @Prof_D_sciences

## Question

Premièrement, je vais raffiner un peu la question : Je me souhaite savoir à quel point les prédictions à 24 h de production du solaire et de l’éolien (Sun and Wind - SW) sont fiables dans différents pays européens sur une période d’un an (année 2019).

Je vais limiter mon analyse à une dizaine de pays pour simplifier les graphiques. Je ne prends pas en compte les bidding zones.

## Méthode

Pour pouvoir répondre à cette question, il faut des data. En termes de data sur la production électrique en Europe, je me tourne systématiquement vers ENTSO-E. Et dans ce cas, ça tombe bien, il y a bien des data sur le sujet !

Il y a deux fonctions pour avoir les prédictions sur une journée :

- [**Generation Forecast - Day Ahead**](https://transparency.entsoe.eu/generation/r2/dayAheadAggregatedGeneration/show)

> An estimate of the total scheduled Net generation (MW) per bidding zone, per each market time unit of the following day. The information shall be published no later than 18h Brussels time, one day before actual delivery takes place.

Les données sont publiées à 18h, pour le jour suivant. Petit souci : c’est agrégé. C’est-à-dire qu’il n’y a pas de catégories, mais un seul chiffre global de production.

- [Generation Forecasts for Wind and Solar](https://transparency.entsoe.eu/generation/r2/dayAheadGenerationForecastWindAndSolar/show)

> A forecast of wind and solar power net generation (MW) per bidding zone, per each market time unit of the following day.
>
> - **Current forecast:** The information published is the last update of the current forecast. The information shall be regularly updated and published during intra-day trading;
>- **Day ahead forecast at 18.00:** The information shall be published no later than 18.00 Brussels time, one day before actual delivery takes place. This value is the most recent forecast at 18:00 the day before;
> - **Intraday forecast at 8.00:** at least one update to be published at 8.00 Brussels time on the day of actual delivery for intra-day. This value is the most recent forecast at 8:00 the day of delivery.
> 
> The information shall be provided for all bidding zones only in Member States with more than 1% feed-in of wind or solar power generation per year or for bidding zones with more than 5% feed-in of wind or solar power generation per year.
>
>  
>
> Forecasts of wind and solar power net generation (MW) per bidding zone, per each market time unit of the following day:
>
> - Current forecast
>- Day ahead forecast at 18.00 
> - Intraday forecast at 8.00
> 
>  
>
> Detailed description of this information:
>
> - Current forecast: The information published is the last update of the forecast. The information shall be regularly updated.
>- Day ahead forecast at 18.00: The information is a fixed value, i.e. the current forecast at 18.00 the day before. The information shall be published no later than 18.00 Brussels time, one day before actual delivery takes place and shall not be regularly updated after 18.00.
> - Intraday forecast at 8.00: The information is a fixed value, i.e. the current forecast at 8.00 the day of delivery. The information shall be published at 8.00 Brussels time on the day of actual delivery for intra-day and shall not be regularly updated after 8.00.
> 
>  
>
> Note: every submission has to be published at least as “current forecast”. For example, the last forecast submitted before 18.00 in D-1 will be published as “day ahead forecast at 18.00” but also as “current forecast”. In the same manner, the forecast published at 8.00 the day of delivery will be published twice, as “current forecast” and “intraday forecast at 8.00” as well.

Ces données décomposent les prévisions du solaire et de l’éolien seulement. On utilisera donc la prévision sur la journée suivante. Une fois ces data récupérées, il ne reste plus qu’à jouer avec.

## Graphs

En premier, regardons de façon brute la production (abscisse) par rapport à la prévision (ordonnée).

![](./graphs/00-Scatter_Production_Forecast.png)

A première vue, comme pour la France, l'ensemble des pays semblent suivre une droite où production est égale à la prédiction. Toutefois, ce n'est pas très claire du fait de la disparité de production. Pour rendre le graphique plus lisible, il est possible de diviser la production (et la prévision) par le maximum de chaque pays.

![](./graphs/01-Scatter_Production_Forecast_Ratio.png)

## Separer les erreurs positive des négatives

Box plots

Valeurs relatives

## Regarder par mois

Box plots ?

Bar plots ?