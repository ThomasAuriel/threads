## Le but

Arriver à savoir à quel point on arrive à bien prédire la production d’énergie des énergies renouvelable (EnR). Le critère est continue et sera probablement un ration ou pourecentage.

## Data Structure

Toutes les données sont stockées dans une base de donnée (SQLite).

### Puissance prédite

La requête suivante permet de connaitre la puissance **prédite** (contenue dans la table generationwindandsolarforcast), pour les pays "BE", "FR", "DE" durant les deux premières heure de l'année 2019 (2019-01-01 00:00:00 et 2019-01-01 01:00:00)

```sql
SELECT * FROM generationwindandsolarforcast WHERE country in ("BE", "FR", "DE") and "date" between "2019-01-01 00:00:00.000000" and "2019-01-01 01:00:00.000000"
```

| country | category      | date                       | eur     |
| ------- | ------------- | -------------------------- | ------- |
| BE      | Solar         | 2019-01-01 00:00:00.000000 | 0.0     |
| BE      | Solar         | 2019-01-01 01:00:00.000000 | 0.0     |
| BE      | Wind Offshore | 2019-01-01 00:00:00.000000 | 557.0   |
| BE      | Wind Offshore | 2019-01-01 01:00:00.000000 | 599.0   |
| BE      | Wind Onshore  | 2019-01-01 00:00:00.000000 | 267.0   |
| BE      | Wind Onshore  | 2019-01-01 01:00:00.000000 | 299.0   |
| DE      | Solar         | 2019-01-01 00:00:00.000000 | 0.0     |
| DE      | Solar         | 2019-01-01 01:00:00.000000 | 0.0     |
| DE      | Wind Offshore | 2019-01-01 00:00:00.000000 | 5042.25 |
| DE      | Wind Offshore | 2019-01-01 01:00:00.000000 | 5028.5  |
| DE      | Wind Onshore  | 2019-01-01 00:00:00.000000 | 20626.5 |
| DE      | Wind Onshore  | 2019-01-01 01:00:00.000000 | 22355.5 |
| FR      | Solar         | 2019-01-01 00:00:00.000000 | 0.0     |
| FR      | Solar         | 2019-01-01 01:00:00.000000 | 0.0     |
| FR      | Wind Onshore  | 2019-01-01 00:00:00.000000 | 1680.0  |
| FR      | Wind Onshore  | 2019-01-01 01:00:00.000000 | 1675.0  |

La colone eur est mal nomée. C'est une puissance en MW qui est représentée, pas un prix. C'est une erreur de ma part, ce sera corrigé à l'avenir.

Ce qu'il faut comprendre sur la structure des données : La puissance prédite est triées en fonction du pays, puis de la source (Solar, Wind Offshore, Wind Onshore)

### Puissance produite

La requête suivante permet de connaitre la puissance **produite** (contenue dans la table generation), pour les pays "BE", "FR", "DE" durant les deux premières heure de l'année 2019 (2019-01-01 00:00:00 et 2019-01-01 01:00:00)

```sql
SELECT * FROM generation WHERE country in ("BE", "FR", "DE") and "date" between "2019-01-01 00:00:00.000000" and "2019-01-01 01:00:00.000000" and "category" in ("Solar", "Wind Onshore", "Wind Offshore")
```

| country | category      | date                       | mw       |
| ------- | ------------- | -------------------------- | -------- |
| BE      | Solar         | 2019-01-01 00:00:00.000000 | 0.0      |
| BE      | Solar         | 2019-01-01 01:00:00.000000 | 0.0      |
| BE      | Wind Offshore | 2019-01-01 00:00:00.000000 | 500.0    |
| BE      | Wind Offshore | 2019-01-01 01:00:00.000000 | 554.0    |
| BE      | Wind Onshore  | 2019-01-01 00:00:00.000000 | 305.0    |
| BE      | Wind Onshore  | 2019-01-01 01:00:00.000000 | 292.0    |
| DE      | Solar         | 2019-01-01 00:00:00.000000 | 0.0      |
| DE      | Solar         | 2019-01-01 01:00:00.000000 | 0.0      |
| DE      | Wind Offshore | 2019-01-01 00:00:00.000000 | 2868.0   |
| DE      | Wind Offshore | 2019-01-01 01:00:00.000000 | 2459.5   |
| DE      | Wind Onshore  | 2019-01-01 00:00:00.000000 | 22036.5  |
| DE      | Wind Onshore  | 2019-01-01 01:00:00.000000 | 22748.25 |
| FR      | Solar         | 2019-01-01 00:00:00.000000 | 0.0      |
| FR      | Solar         | 2019-01-01 01:00:00.000000 | 0.0      |
| FR      | Wind Onshore  | 2019-01-01 00:00:00.000000 | 1637.0   |
| FR      | Wind Onshore  | 2019-01-01 01:00:00.000000 | 1567.0   |

Cette fois la dernière colonne est bien nomée.

Il est nécessaire de faire un trie plus fin dans la requête SQL. Il y a plus de source d'énergie que ces trois là.

## Graphs

Pour le moment j'ai surtout deux graphiques à partir de ces données.

![00-Scatter_Production_Forecast](./graphs/00-Scatter_Production_Forecast.png)

![01-Scatter_Production_Forecast_Ratio](./graphs/01-Scatter_Production_Forecast_Ratio.png)

## Outils ?

A partir de ces data quel pourrait être les indicateurs qui permettent de dire quel est la **qualité** (à définir) de la prédiction de la production ?

Sachant aussi qu'il est possible que cette qualité, varie en fonction de l'heure de la journée, de la saison (hiver/été) ou même de la puissance (faible puissance implique une moins bonne prédiction ?).

Je ne pense pas que cela dépend des autres sources d'énergie car les EnR sont aujourd'hui prioritaires sur le réseau. Donc s'ils produisent, c'est aux autres sources de s'adapter, pas à aux EnR.