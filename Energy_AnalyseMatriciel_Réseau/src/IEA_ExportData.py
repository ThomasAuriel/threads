import json
import os
from functools import reduce

import pandas
import requests
from pandas import DataFrame

from utils import Utils
from utils import FileUtils


def export(data: DataFrame, filepath: str):

    # data = data.reset_index()

    parent = FileUtils.getParent(filepath)
    FileUtils.createFolder(folder=parent)
    data = data.to_csv(filepath)


if __name__ == "__main__":

    # Get global energy consumption
    df_biomass = Utils.getData("./data/electricity/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH.json")
    df_biomass = df_biomass.rename(columns={"value": "Biomass [TWh]"})
    df_coal = Utils.getData("./data/electricity/COAL_ELECTRICITY_GENERATION_TWH.json")
    df_coal = df_coal.rename(columns={"value": "Coal [TWh]"})
    df_elec = Utils.getData("./data/electricity/ELECTRICITY_GENERATION_TWH.json")
    df_elec = df_elec.rename(columns={"value": "Electricity production [TWh]"})
    df_gas = Utils.getData("./data/electricity/GAS_ELECTRICITY_GENERATION_TWH.json")
    df_gas = df_gas.rename(columns={"value": "Gas [TWh]"})
    df_geo = Utils.getData(
        "./data/electricity/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json"
    )
    df_geo = df_geo.rename(columns={"value": "Geo-Solar-Wind-Tidal [TWh]"})
    df_hydro = Utils.getData("./data/electricity/HYDRO_PRODUCTION_MTOE.json")
    df_hydro = df_hydro.rename(columns={"value": "Hydro [TWh]"})
    df_nuc = Utils.getData("./data/electricity/NUCLEAR_PRODUCTION_MTOE.json")
    df_nuc = df_nuc.rename(columns={"value": "Nuclear [TWh]"})
    df_oil = Utils.getData("./data/electricity/OIL_ELECTRICITY_GENERATION_TWH.json").rename(
        columns={"value": "Oil [TWh]"}
    )
    df_pop = Utils.getData("./data/electricity/POPULATION.json")
    df_pop = df_pop.rename(columns={"value": "Population [Million]"})

    dfs = [
        df_biomass,
        df_coal,
        df_elec,
        df_gas,
        df_geo,
        df_hydro,
        df_nuc,
        df_oil,
        df_pop,
    ]

    df_whole = pandas.concat(dfs)

    df_whole = reduce(
        lambda left, right: pandas.merge(left, right, on=["country", "date"], how="outer"),
        dfs,
    )
    df_whole = df_whole.round(2)

    export(df_whole, "./data/export/WholeData.csv")
