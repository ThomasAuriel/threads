from datetime import datetime

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy
import pandas

from utils import FigureUtils, FileUtils, PandasIndexes, PandasSelection, Utils


def share(
    filename,
    listCountry,
):

    ## Collect data
    generation_electricity = Utils.getData(
        filepath="./data/electricity/ELECTRICITY_GENERATION_TWH.json"
    )
    generation_electricity = PandasSelection.select(
        data=generation_electricity, parameter="country", value=listCountry
    )

    generation_coal = Utils.getData(
        filepath="./data/electricity/COAL_ELECTRICITY_GENERATION_TWH.json"
    )
    generation_coal = PandasSelection.select(
        data=generation_coal, parameter="country", value=listCountry
    )
    generation_renewable = Utils.getData(
        filepath="./data/electricity/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json"
    )
    generation_renewable += Utils.getData(
        filepath="./data/electricity/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH.json"
    )
    # generation_renewable += Utils.getData(
    #     filepath="./data/electricity/HYDRO_PRODUCTION_MTOE.json"
    # )
    generation_renewable = PandasSelection.select(
        data=generation_renewable, parameter="country", value=listCountry
    )

    # For control with DAX data
    # MTOE_TO_TWH = 11.63
    # TWH_TO_MTOE = 1 / MTOE_TO_TWH
    # print(generation_electricity * TWH_TO_MTOE)
    # print(generation_coal * TWH_TO_MTOE)
    # print(generation_renewable * TWH_TO_MTOE)

    ## Get only the wanted years
    generation_electricity_2018 = PandasSelection.select(
        data=generation_electricity,
        parameter="date",
        value=datetime(2018, 1, 1).isoformat(),
    )
    generation_electricity_2018 = generation_electricity_2018.droplevel(level="date")
    generation_electricity_2010 = PandasSelection.select(
        data=generation_electricity,
        parameter="date",
        value=datetime(2010, 1, 1).isoformat(),
    )
    generation_electricity_2010 = generation_electricity_2010.droplevel(level="date")

    generation_coal_2018 = PandasSelection.select(
        data=generation_coal, parameter="date", value=datetime(2018, 1, 1).isoformat()
    )
    generation_coal_2018 = generation_coal_2018.droplevel(level="date")
    generation_coal_2010 = PandasSelection.select(
        data=generation_coal, parameter="date", value=datetime(2010, 1, 1).isoformat()
    )
    generation_coal_2010 = generation_coal_2010.droplevel(level="date")

    generation_ReNe_2018 = PandasSelection.select(
        data=generation_renewable,
        parameter="date",
        value=datetime(2018, 1, 1).isoformat(),
    )
    generation_ReNe_2018 = generation_ReNe_2018.droplevel(level="date")
    generation_ReNe_2010 = PandasSelection.select(
        data=generation_renewable,
        parameter="date",
        value=datetime(2010, 1, 1).isoformat(),
    )
    generation_ReNe_2010 = generation_ReNe_2010.droplevel(level="date")

    ## Keep only countries present in both years

    share_coal = (
        generation_coal_2018 / generation_electricity_2018
        - generation_coal_2010 / generation_electricity_2010
    ) * 100
    share_Rene = (
        generation_ReNe_2018 / generation_electricity_2018
        - generation_ReNe_2010 / generation_electricity_2010
    ) * 100

    share = pandas.concat(
        [share_coal, share_Rene], keys=["Coal Share [%]", "Renewable Share [%]"], axis=1
    )
    share = share.replace([numpy.inf, -numpy.inf], numpy.nan).dropna(how="any")
    print(share)

    fig, ax, artists = FigureUtils.getFigure(
        size=(12, 8), dpi=360, author="@Thomas_Auriel", interval=""
    )

    for subset in PandasIndexes.getIndexeUniqueValues(share, level="country"):

        tmp_coal = PandasSelection.select(
            share["Coal Share [%]"], "country", subset
        ).values.flatten()
        tmp_Rene = PandasSelection.select(
            share["Renewable Share [%]"], "country", subset
        ).values.flatten()

        ax.scatter(x=tmp_coal, y=tmp_Rene)

    def annotate_df(row):
        ax.annotate(
            row.name,
            row.values,
            xytext=(10, -5),
            textcoords="offset points",
            size=18,
            color="darkslategrey",
        )

    share.apply(annotate_df, axis=1)
    ax.set_xlabel("Coal Share [%]")
    ax.set_ylabel("Renewable Share [%]")

    ax.axhline(y=0, color="k")
    ax.axvline(x=0, color="k")
    ax.grid(True, which="both")

    plt.title("Evolution of the share of renewable and coal from 2010 to 2018")

    # """ Save the plot """
    FileUtils.createFolder(FileUtils.getParent(filename))
    plt.tight_layout()
    plt.savefig(filename, bbox_extra_artists=(*artists,), bbox_inches="tight")
    plt.close("all")


# Countries to keep in the analysis
listCountry = [
    "AU",
    "BR",
    "CA",
    "CN",
    "DE",
    "EG",
    "ES",
    "FR",
    "GB",
    "ID",
    "IN",
    "IR",
    "IT",
    "JP",
    "KR",
    "MY",
    "MX",
    "NL",
    "NO",
    "PL",
    "RU",
    "SA",
    "SE",
    "TH",
    "TR",
    "TW",
    "US",
    "VN",
    "ZA",
]
share(filename="Step1_Fig2.png", listCountry=listCountry)
listCountry = [
    "AE",
    "AL",
    "AM",
    "AO",
    "AR",
    "AT",
    "AU",
    "AZ",
    "BA",
    "BD",
    "BE",
    "BG",
    "BH",
    "BJ",
    "BN",
    "BO",
    "BR",
    "BW",
    "BY",
    "CA",
    "CD",
    "CG",
    "CH",
    "CI",
    "CL",
    "CM",
    "CN",
    "CO",
    "CR",
    "CU",
    "CY",
    "CZ",
    "DE",
    "DK",
    "DO",
    "DZ",
    "EC",
    "EE",
    "EG",
    "ER",
    "ES",
    "ET",
    "EU",
    "FI",
    "FR",
    "GA",
    "GB",
    "GE",
    "GH",
    "GQ",
    "GR",
    "GT",
    "HN",
    "HR",
    "HT",
    "HU",
    "ID",
    "IE",
    "IL",
    "IN",
    "IQ",
    "IR",
    "IS",
    "IT",
    "JM",
    "JO",
    "JP",
    "KE",
    "KG",
    "KH",
    "KP",
    "KR",
    "KW",
    "KZ",
    "LA",
    "LB",
    "LK",
    "LT",
    "LU",
    "LV",
    "LY",
    "MA",
    "MD",
    "ME",
    "MM",
    "MN",
    "MT",
    "MU",
    "MX",
    "MY",
    "MZ",
    "NA",
    "NE",
    "NG",
    "NI",
    "NL",
    "NO",
    "NP",
    "NZ",
    "OM",
    "PA",
    "PE",
    "PH",
    "PK",
    "PL",
    "PT",
    "PY",
    "QA",
    "RO",
    "RS",
    "RU",
    "SA",
    "SD",
    "SE",
    "SG",
    "SI",
    "SK",
    "SN",
    "SR",
    "SS",
    "SV",
    "SY",
    "TG",
    "TH",
    "TJ",
    "TM",
    "TN",
    "TR",
    "TT",
    "TZ",
    "UA",
    "US",
    "UY",
    "UZ",
    "VE",
    "VN",
    "YE",
    "ZA",
    "ZM",
    "ZW",
]
share(filename="Step1_Fig3.png", listCountry=listCountry)
