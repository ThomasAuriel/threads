from typing import List, Union

from _collections_abc import Iterable

import pandas
from pandas import DataFrame, Series
from utils import FileUtils


def convert_MTOEtoTWh(
    data: DataFrame, powerPlant: str = None, filename: str = "NONE", **kwargs
) -> DataFrame:
    # From : https://github.com/ewoken/world-data-app/blob/b83fd7e0cc696628e2e0218e1f5ff95e4bb2d1d1/dataSources/statistics/sources/converters.js
    GENERATION_TO_CONSUMPTION_FACTOR = 0.38
    MTOE_TO_TWH = 11.63
    TWH_TO_MTOE = 1 / MTOE_TO_TWH
    SHORT_TON_TO_TON = 0.90718474
    CUBIC_FOOT_TO_CUBIC_METER = 0.028316846592

    # TODO add power plant considerations
    POWER_PLANT_EFFICIENCIES = {
        "OIL": 0.35,
        "GAS": 0.45,
        "COAL": 0.33,
        "NUCLEAR": 0.33,
        "HYDRO": 1,  # definition of statistics
        "SOLAR": 1,  # same here
        "GEOTHERMY": 0.1,
        "BIOFUELS_WASTE": 0.33,
        "NONE": 1,
    }

    powerplantCoef = 1
    # if powerPlant:
    #     powerplantCoef = POWER_PLANT_EFFICIENCIES[powerPlant]
    # else:
    #     for key in POWER_PLANT_EFFICIENCIES.keys():
    #         if key in filename:
    #             powerplantCoef = POWER_PLANT_EFFICIENCIES[key]
    #             break

    data = data * MTOE_TO_TWH * powerplantCoef

    return data


def getData(filepath: str, removeNaN: bool = True, **kwargs) -> DataFrame:

    data = pandas.read_json(filepath)
    if removeNaN:
        data = data.dropna()
    data = data.rename(
        columns={
            "countryCode": "country",
            "year": "date",
        }
    )
    data["date"] = pandas.to_datetime(data["date"], format="%Y", errors="ignore", utc=True)
    data = data.set_index(keys=["country", "date"])

    if "MTOE" in filepath:
        data = convert_MTOEtoTWh(data, filename=filepath, **kwargs)

    return data
