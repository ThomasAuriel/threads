import json
import os

import pandas
import requests

from utils import FileUtils


def getData(url: str, filepath: str, replaceIfExist: bool = False, verbose: bool = True):
    """
    Get data from IEA
    """

    if replaceIfExist or not os.path.exists(filepath):

        if verbose:
            print("Collect data")
        resp = requests.get(url=url)
        data = resp.json()
        data = json.dumps(data, indent=4, sort_keys=True)  # Get the data

        if verbose:
            print("Save data : " + filepath)
        parent = FileUtils.getParent(filepath)
        FileUtils.createFolder(folder=parent)
        FileUtils.writeFile(filepath=filepath, fileContent=data)
    else:
        if verbose:
            print("Do not replace existing file : " + filepath)


if __name__ == "__main__":

    # Download all available series
    url = "https://ewoken.github.io/world-data-app/data/statistics.json"
    filepath = "./data/electricity/series.json"
    getData(url=url, filepath=filepath)

    # Population
    url = "https://ewoken.github.io/world-data-app/data/POPULATION/all.json"
    filepath = "./data/electricity/electricity/POPULATION.json"
    getData(url=url, filepath=filepath)

    # Electricity generation
    ## All
    url = "https://ewoken.github.io/world-data-app/data/ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Oil
    url = "https://ewoken.github.io/world-data-app/data/OIL_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/OIL_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Gas
    url = "https://ewoken.github.io/world-data-app/data/GAS_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/GAS_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Coal
    url = "https://ewoken.github.io/world-data-app/data/COAL_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/COAL_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## Biomass
    url = "https://ewoken.github.io/world-data-app/data/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
    ## EnR
    url = "https://ewoken.github.io/world-data-app/data/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH/all.json"
    filepath = "./data/IEA/electricity/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json"
    getData(url=url, filepath=filepath)
