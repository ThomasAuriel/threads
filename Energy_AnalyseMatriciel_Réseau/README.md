# Graphique du DRAX

Le DRAX a produit un graphique dans son rapport : [Electric Insights Quarterly – July to September 2020](https://reports.electricinsights.co.uk/wp-content/uploads/2020/11/201126_Drax_20Q3_005-1.pdf).

![Figure 1 — Figure du DRAX](./Fig1.jpg)

Ce graphique représente l’évolution du mix électrique de plusieurs pays répartis un peu partout dans le monde. Il cherche notamment à vérifier la relation entre la variation de la part du mix d’électricité issue du charbon et de la part du mix d’électricité issue de l’électricité renouvelable.

Chaque point a pour coordonnées :

$$
x = P_{charbon~2018}/P_{total~2018}-P_{charbon~2010}/P_{total~2010}
$$
$$
y = P_{renouvelable~2018}/P_{total~2018}-P_{renouvelable~2010}/P_{total~2010}
$$

Les pays à gauche de l’axe vertical voient leur part de charbon réduire, et ceux à droite voient leur part augmenter. Les pays au-dessus de l’axe horizontal voient leur part de production d’électricité renouvelable augmenter, et ceux en dessous voient leur part diminuer. Ce graphique utilise une base 2010.

Enfin, une flèche verte vient indiquer une trajectoire où les renouvelables viennent replacer le charbon.

La définition du graphique est problématique, ainsi que la flèche.

## Note sur le rapport

Le rapport contient de nombreuses informations et est rapide à lire. Je le recommande vivement. On apprend beaucoup sur la gestion du réseau en cette période de COVID.

J’en ai d’ailleurs fait un thread à la volée : https://twitter.com/Thomas_Auriel/status/1334072806301200389. (ATTENTION : le thread a été fait sur un téléphone, des erreurs se sont glissées [je confonds charbon et gaz à un moment donné] et pas mal de fautes d’orthographe et de grammaire sont aussi présentes. C’est simple sur téléphone, je n’y arrive pas).

# Collecte des données

Les données sont collectées depuis le site de l’IEA (https://www.iea.org/sankey/) par l’intermédiaire du site (https://ewoken.github.io/world-data-app/data/).

On peut comparer les données de l’IEA avec celle du DRAX (que UK) grâce aux annexes du rapport ici : https://www.gov.uk/government/statistics/digest-of-uk-energy-statistics-dukes-2020

# Reproduction du graph

Malheureusement, il n’est pas possible de reproduire exactement le graphique. Cela est dû au fait, que l’IEA arrête ces données en 2018 et non pas en 2019. J’ai pu contrôler que le DRAX et l’IEA ne compte pas la même chose pour les renouvelables.

Le DRAX inclut dans les renouvelables :

-   Éolien
-   Solaire
-   Marée
-   Géothermie
-   Biomasse

L’IEA ne compte pas la biomasse. Elle l’inclut avec les déchets valorisés (sans faire la distinction entre les deux). En conséquence, ma liste d’énergie basant sur les données de l’IEA est :

-   Éolien
-   Solaire
-   Marée
-   Géothermie
-   Biomasse
-   Waste

Je peux donc reproduire un graphique avec pour différences notables de ne pas considérer 2019 et d’inclure l’électricité générée à partir des déchets. Les niveaux ne sont pas tout à fait les mêmes que ceux obtenus par le DRAX, mais la comparaison est possible.

**Figure 2 — part du mix — base 2010**
![Figure 2 — part du mix — base 2010](./Step1_Fig2.png)

Il y a plusieurs remarques à faire sur ce graphique (par ordre d’importance à mes yeux) :

1.  La liste de pays n’est pas exhaustive.
2.  On compare 2 années. Les données sont disponibles pour plusieurs années. On peut dessiner des trajectoires au cours des années.
3.  2010 est choisi de façon arbitraire comme point de départ du graphique. Suivant l’année de départ, le diagnostic de « bon » ou « mauvais » élève peut changer.
4.  On compare l’évolution de la part du mix. Or cette évolution dépend du mix du point de départ.
5.  On omet toutes les autres sources d’énergie (gaz, pétrole, nucléaire, importation).
6.  On omet toutes les réductions de demande (moins de demandes en absolue, flexibilité).

# Critiques du graphique

## Liste de pays non exhaustive

Pour la liste des pays incluant et du charbon et des renouvelables, il est facile avec les données de l’IEA d’ajouter tous les pays entre 2010 et 2018 qui comportent des renouvelables et du charbon.

**Figure 3 — part du mix pour l’ensemble des pays disponibles dans les données IEA — base 2010**
![Figure 3 — part du mix pour l’ensemble des pays disponibles dans les données IEA — base 2010](./Step1_Fig3.png)

De la forme du nuage de point, on ne constate pas grand-chose à l’œil. Des pays qui ont installé plus de renouvelables relativement à leur mix de 2010 que le Royaume-Uni (GB), il y en a (DK, LU, LT[^LT]). Pourtant, le charbon n’a pas réduit pour autant (toujours relativement à leur mix).
Les pays qui on réduit plus ou autant le charbon que le Royaume-Uni, sont peu nombreux, mais ils existent (GR, IL). Ils ont installé plus ou moins d’énergie renouvelable. Et enfin, des pays qui augmentent leur part de charbon et leurs électricités renouvelables, il y en a aussi. À première vue, il est difficile de tirer des informations de ce graphique.

## Comparaison de deux années

Je ne comprends pas pourquoi se limiter à ces deux années. Il est toujours préférable de comparer des séries longues, quitte à identifier des dynamiques particulières au sein de ces séries. Il est plus intéressant de voir si une trajectoire existe. Si aucune trajectoire ne se dessine alors on peut conclure que ce que met en avant le graphique n’est possible que parce qu’ils ont choisi ces dates.

**Figure 4 — Évolution de la part du mix — base 2010**
![Figure 4 — Évolution de la part du mix — base 2010](./Step2_Fig4.png)

Les trajectoires des différents pays apportent plusieurs informations.

Plusieurs pays semblent suivre des trajectoires voisines. On peut noter le groupe (GB, DE, IT) et le groupe (AU, CN, PL, US). Le groupe (GB, DE, IT) se traduit par une augmentation des renouvelables puis une réduction de la part de charbon. Tant dit que l’ensemble (AU, CN, PL, US) montre une augmentation des renouvelables accompagnants une réduction du charbon un peu chaotique par moment.

Ce qu’on peut observer pour le Royaume-Uni, c’est une forte diminution de l’utilisation du charbon à partir de 2012. Or l’augmentation des énergies renouvelables ne date pas de cette époque. Il n’y a pas une trajectoire directe comme la flèche verte de la première figure le laisse entendre. De plus, la relation entre énergie renouvelable et charbon n’est pas directe. Ils ont perdu -19,7 % de part de charbon dans leur mixe et gagné 24,7 % de renouvelables.


## L’an 2000 comme point de départ ?

En prenant 2010 pour point de départ, cette conclusion semble tout à fait logique. Toutefois, rien ne justifie cette date foot[^2010]. En prenant, tout aussi arbitrairement, l’an 2000, on obtient les graphiques suivants.

**Figure 5 — Évolution de la part du mix — base 2000**
![Figure 5 — Évolution de la part du mix — base 2000](./Step3_Fig5.png)

Ici encore on constate que changer le point de départ change la distribution des pays. Les groupes ne sont plus les mêmes. L’Espagne est subitement devenue un « bon élève » bien qu’hésitant sur la fin. Le groupe (AU, CR, PL, US) se retrouve plus distendu.

Prendre une année de départ permet de comparer des dynamiques d’individus (ici des pays) entre elles. On peut même prendre des dates de départ différentes pour chaque pays. Mais il faut justifier pourquoi une date est privilégiées plutôt qu’une autre. Ici on ne sait pas. On peut seulement soupçonner que 2010-2019 c’est 10 ans.

## Comparaison de part de mix, de production relative et production absolue

Depuis le début, on compare des pars de mixe. Ainsi un pays qui baisse fortement sa part du mix sera bien plus écarté de l’axe vertical (comme GB). Tant dis qu’un autre qui installe beaucoup de renouvelables verra sa part de mix montée rapidement.

Cette façon de faire est gênante pour une raison simple : tous les pays ne commencent pas avec le même mix énergétique. Un pays avec beaucoup de renouvelables en 2010 ne pourra pas monter fortement. Ceux avec peu de charbon ne pourront pas le réduire fortement. Une façon de faire complémentaire est de prendre la production absolue par rapport au nombre d’habitants. Cela permet de comparer des pays de population inégale.

**Figure 6 — Comparaison de la production par habitant - base 2010**
![Figure 6 — Comparaison de la production par habitant - base 2010](./Step4_Fig6.png)
**Figure 7 — Comparaison de la production par habitant - base 2000**
![Figure 7 — Comparaison de la production par habitant - base 2000](./Step5_Fig7.png)

Encore une fois, on voit ici que les différents pays se retrouvent dans des groupes encore différents.

- La Chine jusqu’ici « bon » élève, se retrouve dans les « mauvais ».
- On peut constater que l’Espagne change de « bon » ou « mauvais » élève suivant l’année de base.
- Enfin, le groupe (AU, CN, PL, US) n’existe plus. À la place, l’on a (US, AU, CA).

Utiliser une année de comparaison est donc très influent sur le résultat et les conclusions qu’on peut tirer sur les politiques de chaque pays.

Il est possible de retirer l’année de comparaison. On a alors la variation dans son contexte. C’est-à-dire qu’un pays qui comme l’Allemagne utilise 3 MWh par habitant en 2000 d’électricité au charbon pourra réduire plus fortement son utilisation qu’un pays comme le Royaume-Uni qui en utilisait que 2 MWh par habitant.

**Figure 8 — Comparaison de la production par habitant**
![Figure 8 — Comparaison de la production par habitant](./Step6_Fig8.png)

Pour revenir à la comparaison de mixes, on comprend que certains pays ont plus de marges pour réduire leur part de mix au charbon que d’autres. La Chine, la Pologne et l’Afrique du Sud ne tournaient pratiquement qu’au charbon en 2000, tandis que, la Norvège, La France, le Brésil et la Suède ont très peu de charbon dans leur mix dès les années 2000.

**Figure 9 — Comparaison de la part du mix**
![Figure 9 — Comparaison de la part du mix](./Step7_Fig9.png)

## Les autres sources d’énergie et de réduction de l’utilisation de charbon

Le problème de ces graphiques, c’est qu’on ne peut pas tirer d’information claire sur ce qui se passe. Les pays de l’OCDE ne voient pas leur réseau électrique grossir significativement. En déployant de nouvelles sources, on remplace de plus anciennes. Par contre dans des pays en voie de développement, bien que la part du mix se réduise pour le charbon comme en Chine, la consommation totale augmente toujours.

## Conclusion sur ce graphique

Pour conclure, ce qu’on peut dire de la flèche verte du premier graphique c’est quelle suppose une relation directe entre renouvelable et charbon, au moins dans le mix électrique anglais. Cette relation n’est pas valable que sur une comparaison du mix électrique. Si on prend par exemple 2012 comme point de départ de la réduction de charbon, alors on passe de 1,97 MWh/hab consommé à 0,25 MWh/hab (-1,72 MWh/hab) tandis que les renouvelables montent de 0,61 MWh/hab à 1,56 MWh/hab (+0,95 MWh/hab).

La variation de renouvelable n’explique pas à elle seule la variation de charbon dans le mix anglais.

**Figure 10 — Comparaison de la part du mix**
![Figure 10 — Comparaison de la part du mix](./Step8_Fig10.png)

**Table 1 — Valeur de la figure 10**
| Année | Charbon consommé [MWh/hab] | Renouvellable consommé [MWh/hab] |
| ----- | -------------------------- | -------------------------------- |
| 2000 |  1.870318 | 0.104466 |
| 2001 |  2.038409 | 0.143015 |
| 2002 |  1.913440 | 0.161804 |
| 2003 |  2.078277 | 0.180355 |
| 2004 |  1.996110 | 0.211515 |
| 2005 |  2.045992 | 0.261092 |
| 2006 |  2.232847 | 0.278294 |
| 2007 |  2.034030 | 0.288842 |
| 2008 |  1.782124 | 0.311604 |
| 2009 |  1.448235 | 0.365093 |
| 2010 |  1.491958 | 0.393001 |
| 2011 |  1.516745 | 0.518267 |
| 2012 |  1.970156 | 0.605780 |
| 2013 |  1.777463 | 0.758790 |
| 2014 |  1.360446 | 0.908584 |
| 2015 |  1.031437 | 1.150744 |
| 2016 |  0.432857 | 1.183002 |
| 2017 |  0.319540 | 1.397878 |
| 2018 |  0.248313 | 1.558125 |

Il est également à noter que depuis le début je me cantonne à 2018 comme date de fin. Il s’avère que le DRAX a publié les datas pour le Royaume-Uni en 2019 (https://www.gov.uk/government/statistics/digest-of-uk-energy-statistics-dukes-2020 — Table 5.3). On peut identifier une forte baisse du charbon sans que les renouvelables ne compensent cette variation.

**Table 2 — Valeur issue du rapport du DRAX**

| All generating companies<br />Millions of tonnes of oil equivalent | 2017  | 2018  | 2019  | Variation 2018–2019 |
| ------------------------------------------------------------ | ----- | ----- | ----- | ------------------- |
| Coal                                                         | 5.55  | 4.24  | 1.85  | -2.39               |
| Oil                                                          | 0.54  | 0.49  | 0.39  | -0.1                |
| Gas                                                          | 24.6  | 23.51 | 23.42 | -0.09               |
| Nuclear                                                      | 15.12 | 14.06 | 13.25 | -0.81               |
| Hydro (natural flow)                                         | 0.51  | 0.47  | 0.51  | 0.04                |
| Wind, wave and solar photovoltaics                           | 5.25  | 5.99  | 6.64  | 0.65                |
| Other renewables                                             | 7.99  | 8.82  | 9.5   | 0.68                |
| Other fuels                                                  | 2.14  | 2.28  | 2.51  | 0.23                |
| Net imports                                                  | 1.27  | 1.64  | 1.82  | 0.18                |
| Total all generating companies                               | 62.98 | 61.50 | 59.89 | -1.61               |

# Conclusion

Le graphique ne permet pas de dire que le Royaume-Uni suit la trajectoire qu’il pointe avec la flèche verte. Pour cela, il est nécessaire de faire une analyse statistique.

Vous noterez que je n’ai pas parlé des deux derniers points problématiques que j’ai identifiés sous la figure 2. Cela fera l’objet d’une étude supplémentaire. J’en profiterai pour vous expliquer un outil de statistique que j’aime particulièrement.

[^2010]: Le DRAX est une centrale thermique au Royaume-Uni de 3600 MW (6 * 600 MW). Elle a été construite progressivement dans les années 70 et fonctionnait au charbon. En 2010, elle est passée du charbon à la biomasse. Le groupe qui la gère DRAX Power (https://www.drax.com/) est à l’origine du rapport et du graphique. On peut imaginer que le choix de l’année 2010 vient de là.

[^LT]: La Lituanie a connu une évolution particulière de sa production électrique. Elle a réduit considérablement sa production électrique (notamment nucléaire) au profit des importations. Ce qu’il reste de disponible, c’est une faible production électrique et une forte cogénération (production de chaleur dans des centrales thermiques) à base de biomasse notamment (60%). Pour équilibrer la demande, la Lituanie se base sur de nombreuses importations.