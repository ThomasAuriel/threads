from _collections_abc import Iterable
from datetime import datetime
from typing import Callable, Dict, List, Union

import numpy
import pandas
from pandas import DataFrame
from pandas.core.groupby import GroupBy
from pandas.core.series import Series

from utils import PandasIndexes


def select(
    data: DataFrame,
    parameter: Union[List[str], str],
    value: Union[List[str], str],
    operator: str = "or",
) -> DataFrame:
    """
    Select data following the parameter and the value provided

    data:
        data to filter
    country:
        country to select
    """

    if isinstance(data, Series):
        data = data.to_frame()

    queryString = ""
    if (
        isinstance(parameter, Iterable)
        and not isinstance(parameter, str)
        and isinstance(value, Iterable)
        and not isinstance(value, str)
    ):
        zipValues = list(zip(parameter, value))
        queryString = (" " + operator + " ").join([p + ' == "' + v + '"' for p, v in zipValues])
    elif isinstance(parameter, str) and isinstance(value, Iterable) and not isinstance(value, str):
        queryString = (" " + operator + " ").join(
            [str(parameter) + ' == "' + str(v) + '"' for v in value]
        )
    else:
        queryString = str(parameter) + ' == "' + str(value) + '"'

    data = data.query(queryString)
    return data


def selectBetween(
    data: DataFrame,
    parameter: str,
    start: Union[str, datetime],
    end: Union[str, datetime],
    includeStart: bool = True,
    includeEnd: bool = True,
):
    """
    Select data following the parameter and the value provided

    data:
        data to filter
    country:
        country to select
    """

    wasSerie = False
    previousUnit = None
    if isinstance(data, Series):
        wasSerie = True
        data = data.to_frame()

    if isinstance(start, datetime):
        start = start.isoformat()
    if isinstance(end, datetime):
        end = end.isoformat()

    queryString = ""
    if includeStart:
        queryString += str(parameter) + ' >= "' + str(start) + '"'
    else:
        queryString += str(parameter) + ' > "' + str(start) + '"'

    queryString += " and "

    if includeEnd:
        queryString += str(parameter) + ' <= "' + str(end) + '"'
    else:
        queryString += str(parameter) + ' < "' + str(end) + '"'

    data = data.query(queryString)

    if wasSerie:
        # Return a Series
        data = data.iloc[:, 0]

    return data


def dropEmptyIndex(data: DataFrame, level: List[str], verbose: bool = False, **kwargs):

    dataGroup = data.groupby(level=level)

    toReturn = []
    for key, group in dataGroup:

        # Method too slow
        #         if not group.any():
        #
        #             if verbose:
        #                 print("Drop : " + str(key))
        #
        #             data.drop(key, inplace=True)
        # #             data = data[~data.index.isin(key)]

        if group.any():

            if verbose:
                print("Keep : " + str(key))

            toReturn.append(group)

    toReturn = pandas.concat(toReturn)
    toReturn.name = data.name

    return toReturn


def getCommonCountries(*datas: List[DataFrame]):

    countries = None
    for idx in range(len(datas)):
        if countries is None:
            countries = PandasIndexes.getIndexeUniqueValues(datas[0], index.country)
        else:
            countries = countries.intersection(
                other=PandasIndexes.getIndexeUniqueValues(datas[idx], index.country),
                sort=False,
            )

    return countries


def selectCountries(*datas: List[DataFrame]):

    countries = getCommonCountries(*datas)
    return tuple([selectCountry(data, countries) for data in datas])
