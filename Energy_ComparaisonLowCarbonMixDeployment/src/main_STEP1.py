from datetime import datetime

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy
import pandas

import Data

from utils import FigureUtils, FileUtils, PandasIndexes, PandasSelection, Utils

# Settings
filename = "./output/test.png"
nbCategories = 20
averagePeriode = 5

## Collect data
generation = Data.getGeneration(listCountry=None)
generation = generation.groupby(level=["country", "category"]).apply(
    # lambda x: x.diff(periods=10).replace(numpy.inf, 0)
    lambda x: x.diff()
    .rolling(window=averagePeriode, min_periods=averagePeriode)
    .mean()
)
generation = generation.sort_values(ascending=False, by="value").head(nbCategories)

# Change label
generation["index"] = range(nbCategories)

categories = generation.index.get_level_values("category")
generation["label"] = [c for c in categories + " "]

countries = generation.index.get_level_values("country")
generation["label"] += [c + " " for c in countries]

dates = generation.index.get_level_values("date")
generation["label"] += [str(year - averagePeriode) + "-" + str(year) for year in dates.year]


# Create figure
fig, ax, artists = FigureUtils.getFigure(
    size=(12, 8), dpi=360, author="@Thomas_Auriel\nsource IEA", interval=""
)

for category in PandasIndexes.getIndexeUniqueValues(generation, level="category"):
    ax.barh(
        y=generation["index"].loc[category],
        width=generation["value"].loc[category],
        color=Data.colorCycle[category],
    )


def annotate_df(row):
    ax.annotate(
        row.name,
        row.values,
        xytext=(10, -5),
        textcoords="offset points",
        size=18,
        color="darkslategrey",
    )


# share.apply(annotate_df, axis=1)
ax.set_xlabel("Added generation [kWh/cap/yr]")
ax.set_yticks(generation["index"])
ax.set_yticklabels(generation["label"])

# ax.axhline(y=0, color="k")
# ax.axvline(x=0, color="k")
ax.grid(True, which="both")

plt.title(
    "Added generation\n"
    + str(averagePeriode)
    + " years average generation per capita added per year"
)

artists.append(FigureUtils.addLegend(ax))

# """ Save the plot """
FileUtils.createFolder(FileUtils.getParent(filename))
plt.tight_layout()
plt.savefig(
    filename,
    bbox_extra_artists=(*artists,),
    bbox_inches="tight",
)
plt.close("all")
