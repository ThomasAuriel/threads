from typing import List, Union

import pandas
from pandas import DataFrame

from utils import Utils, PandasSelection


colorCycle = {
    "biomass": "#1e8449",
    "coal": "#4e342e",
    "gas": "tab:brown",
    "oil": "tab:gray",
    "nuclear": "tab:olive",
    "geothermal": "tab:red",
    "hydro": "tab:purple",
    "renewable": "tab:blue",
}


def defineColorCycle(categories):

    colorCycle = [
        colorCycle[category]
        if category in colorCycle
        else "#" + "".join([random.choice("0123456789ABCDEF") for index in range(6)])
        for category in categories
    ]
    matplotlib.rcParams["axes.prop_cycle"] = cycler(color=colorCycle)

    return colorCycle


def getGeneration(listCountry: Union[List[str], None]) -> DataFrame:
    ### Population
    population = Utils.getData(filepath="./data/IEA/POPULATION.json")

    ### Electricity

    generation = {}
    generation["coal"] = (
        Utils.getData(filepath="./data/IEA/COAL_ELECTRICITY_GENERATION_TWH.json") / population
    )
    generation["gas"] = (
        Utils.getData(filepath="./data/IEA/GAS_ELECTRICITY_GENERATION_TWH.json") / population
    )
    generation["oil"] = (
        Utils.getData(filepath="./data/IEA/OIL_ELECTRICITY_GENERATION_TWH.json") / population
    )
    generation["renewable"] = (
        Utils.getData(filepath="./data/IEA/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json")
        / population
    )
    generation["biomass"] = (
        Utils.getData(filepath="./data/IEA/BIOFUELS_WASTE_ELECTRICITY_GENERATION_TWH.json")
        / population
    )
    generation["hydro"] = (
        Utils.getData(filepath="./data/IEA/HYDRO_PRODUCTION_MTOE.json") / population
    )
    generation["nuclear"] = (
        Utils.getData(filepath="./data/IEA/NUCLEAR_PRODUCTION_MTOE.json") / population
    )

    generation = pandas.concat(generation, names=["category"])
    if listCountry:
        generation = PandasSelection.select(generation, parameter="country", value=listCountry)
    return generation
