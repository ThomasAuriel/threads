from datetime import datetime

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy
import pandas

import Data

from utils import FigureUtils, FileUtils, PandasIndexes, PandasSelection, Utils

# Settings
filename = "./output/test_{index}.png"

## Collect data
listCountry = ["US", "GB", "FR", "DE", "RU", "CN"]
generation = Data.getGeneration(listCountry)

# # Create figures
# for index in PandasIndexes.getIndexeUniqueValues(generation, level="category"):
#     # Create figure
#     fig, ax, artists = FigureUtils.getFigure(
#         size=(12, 8), dpi=360, author="@Thomas_Auriel\nsource IEA", interval=""
#     )

#     tmp_category = PandasSelection.select(generation, "category", index)

#     for c in PandasIndexes.getIndexeUniqueValues(generation, level="country"):
#         tmp = PandasSelection.select(tmp_category, "country", c)

#         x = PandasIndexes.getIndexeUniqueValues(tmp, level="date")
#         y = tmp.values.flatten()

#         ax.plot(x, y, label=c)

#     def annotate_df(row):
#         ax.annotate(
#             row.name,
#             row.values,
#             xytext=(10, -5),
#             textcoords="offset points",
#             size=18,
#             color="darkslategrey",
#         )

#     # share.apply(annotate_df, axis=1)
#     ax.set_ylabel("Generation [kWh/cap]")

#     # ax.axhline(y=0, color="k")
#     # ax.axvline(x=0, color="k")
#     ax.grid(True, which="both")

#     plt.title("Evolution of the " + index + " in electricity production")

#     artists.append(FigureUtils.addLegend(ax))

#     # """ Save the plot """
#     FileUtils.createFolder(FileUtils.getParent(filename.format_map({"index": index})))
#     plt.tight_layout()
#     plt.savefig(
#         filename.format_map({"index": index}),
#         bbox_extra_artists=(*artists,),
#         bbox_inches="tight",
#     )
#     plt.close("all")


# Create figures
for index in PandasIndexes.getIndexeUniqueValues(generation, level="country"):
    # Create figure
    fig, ax, artists = FigureUtils.getFigure(
        size=(12, 8), dpi=360, author="@Thomas_Auriel\nsource IEA", interval=""
    )

    tmp_category = PandasSelection.select(generation, "country", index)

    for c in PandasIndexes.getIndexeUniqueValues(generation, level="category"):
        tmp = PandasSelection.select(tmp_category, "category", c)

        x = PandasIndexes.getIndexeUniqueValues(tmp, level="date")
        y = tmp.values.flatten()

        ax.plot(x, y, label=c)

    def annotate_df(row):
        ax.annotate(
            row.name,
            row.values,
            xytext=(10, -5),
            textcoords="offset points",
            size=18,
            color="darkslategrey",
        )

    # share.apply(annotate_df, axis=1)
    ax.set_ylabel("Generation [kWh/cap]")

    # ax.axhline(y=0, color="k")
    # ax.axvline(x=0, color="k")
    ax.grid(True, which="both")

    plt.title("Evolution of the " + index + " in electricity production")

    artists.append(FigureUtils.addLegend(ax))

    # """ Save the plot """
    FileUtils.createFolder(FileUtils.getParent(filename.format_map({"index": index})))
    plt.tight_layout()
    plt.savefig(
        filename.format_map({"index": index}),
        bbox_extra_artists=(*artists,),
        bbox_inches="tight",
    )
    plt.close("all")
