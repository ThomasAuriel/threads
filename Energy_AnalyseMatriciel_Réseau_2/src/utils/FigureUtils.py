from typing import Any, List, Tuple

import matplotlib
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure, subplot

matplotlib.use("agg")


def getFigure(
    size: Tuple[int, int],
    interval: str,
    author: str,
    projection: str = "2d",
    grid: bool = True,
    **kwargs
) -> Tuple[figure, subplot, List[Any]]:

    fig = plt.figure(figsize=size)
    #     fig.set_size_inches(size)

    artists: List[Any] = []

    plt.style.context("fast")

    # Define a subplot to be used
    ax = None

    if projection == "2d":
        ax = fig.add_subplot(1, 1, 1)

        a = ax.text(
            1,
            1.00,
            author,
            horizontalalignment="right",
            verticalalignment="bottom",
            transform=ax.transAxes,
        )
        if interval:
            a = ax.text(
                0.00,
                1.00,
                interval,
                horizontalalignment="left",
                verticalalignment="bottom",
                transform=ax.transAxes,
            )
            artists.append(a)

        # Horizontal grid
        ax.yaxis.grid(grid)

        artists.append(a)

    elif projection == "3d":
        ax = fig.add_subplot(111, projection="3d")

    #         ax.text(1, 1.01, s=author,
    #             horizontalalignment='right',
    #             verticalalignment='bottom',
    #             z='top',
    #             transform=ax.transAxes)

    return fig, ax, artists


def addLegend(ax):

    lgd = ax.legend(loc="upper left", bbox_to_anchor=(1, 1), ncol=1, markerscale=2)

    # Change country name in the legend
    for idx, text in enumerate(lgd.get_texts()):
        text.set_text(text.get_text())

    return lgd
