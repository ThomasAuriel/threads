from datetime import datetime

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy
import pandas
from sklearn import preprocessing
from sklearn.decomposition import PCA

import Data

# from utils import FigureUtils, FileUtils, PandasIndexes, PandasSelection, Utils

# Settings
filename = "./output/test_{index}.png"


## Collect data
listCountry = ["US", "GB", "FR", "DE", "RU", "CN"]
# listCountry = None
generation = Data.getGeneration(listCountry)

## Prepare generation
generation = generation.reset_index()

generation["label"] = generation["date"].dt.strftime("%Y") + " " + generation["country"]
generation = generation.drop(["date", "country"], axis=1)
generation = generation.set_index("label").pivot_table(index=["label"], columns=["category"])
generation = generation.dropna()

print(generation)

n = generation.shape[1]
n = 5
p = generation.shape[1]


# Centrage et Réduction
std_scale = preprocessing.StandardScaler().fit(generation)
generation_fit = std_scale.transform(generation)

# Calcul des composantes principales
acp = PCA(n_components=n)
coord = acp.fit_transform(generation)

## Variance expliquée
print("Variance expliquée")
print(acp.explained_variance_)
print("Valeur propres")
eigval = (n - 1) / n * acp.explained_variance_
print(eigval)
print(acp.singular_values_ ** 2 / n)

# proportion de variance expliquée
print("Variance des axes")
print(acp.explained_variance_ratio_)

# Eboulis des valeurs propres
plt.plot(numpy.arange(1, n + 1), eigval)
plt.title("Screen plot")
plt.ylabel("Eigen values")
plt.xlabel("Factor number")
plt.show()

# cumul de variance expliquée
plt.plot(numpy.arange(1, n + 1), numpy.cumsum(acp.explained_variance_ratio_))
plt.title("Explained variance vs. # of factors")
plt.ylabel("Cumsum explained variance ratio")
plt.xlabel("Factor number")
plt.show()


# Positionnement des individus dans le plan
fig, axes = plt.subplots(figsize=(12, 12))
xmin = numpy.min(generation_fit[:, 0])
xmax = numpy.max(generation_fit[:, 0])
ymin = numpy.min(generation_fit[:, 1])
ymax = numpy.max(generation_fit[:, 1])

axes.set_xlim(xmin, xmax)  # même limites en abscisse
axes.set_ylim(ymin, ymax)  # et en ordonnée
# placement des étiquettes des observations
plt.scatter(coord[:, 0], coord[:, 1])
# for i in range(len(generation.index)):
#     if i % 10 == 0:
#         plt.annotate(generation.index[i], (generation_fit[i, 0], generation_fit[i, 1]))

# ajouter les axes
plt.plot([xmin, xmax], [0, 0], color="silver", linestyle="-", linewidth=1)
plt.plot([0, 0], [ymin, ymax], color="silver", linestyle="-", linewidth=1)

# affichage
plt.show()


sqrt_eigval = numpy.sqrt(eigval)
# corrélation des variables avec les axes
corvar = numpy.zeros((p, p))
for k in range(n):
    corvar[:, k] = acp.components_[k, :] * sqrt_eigval[k]
# afficher la matrice des corrélations variables x facteurs
print(corvar)


# cercle des corrélations
fig, axes = plt.subplots(figsize=(8, 8))
axes.set_xlim(-1, 1)
axes.set_ylim(-1, 1)
# affichage des étiquettes (noms des variables)
for j in range(p):
    plt.annotate(generation.columns[j], (corvar[j, 0], corvar[j, 1]))
# ajouter les axes
plt.plot([-1, 1], [0, 0], color="silver", linestyle="-", linewidth=1)
plt.plot([0, 0], [-1, 1], color="silver", linestyle="-", linewidth=1)

# affichage
plt.show()

#
#
#
#
#
#
#
#
#
#
#
#
#


# # Create figures
# for index in PandasIndexes.getIndexeUniqueValues(generation, level="country"):
#     # Create figure
#     fig, ax, artists = FigureUtils.getFigure(
#         size=(12, 8), dpi=360, author="@Thomas_Auriel\nsource IEA", interval=""
#     )

#     tmp_category = PandasSelection.select(generation, "country", index)

#     for c in PandasIndexes.getIndexeUniqueValues(generation, level="category"):
#         tmp = PandasSelection.select(tmp_category, "category", c)

#         x = PandasIndexes.getIndexeUniqueValues(tmp, level="date")
#         y = tmp.values.flatten()

#         ax.plot(x, y, label=c)

#     def annotate_df(row):
#         ax.annotate(
#             row.name,
#             row.values,
#             xytext=(10, -5),
#             textcoords="offset points",
#             size=18,
#             color="darkslategrey",
#         )

#     # share.apply(annotate_df, axis=1)
#     ax.set_ylabel("Generation [kWh/cap]")

#     # ax.axhline(y=0, color="k")
#     # ax.axvline(x=0, color="k")
#     ax.grid(True, which="both")

#     plt.title("Evolution of the " + index + " in electricity production")

#     artists.append(FigureUtils.addLegend(ax))

#     # """ Save the plot """
#     FileUtils.createFolder(FileUtils.getParent(filename.format_map({"index": index})))
#     plt.tight_layout()
#     plt.savefig(
#         filename.format_map({"index": index}),
#         bbox_extra_artists=(*artists,),
#         bbox_inches="tight",
#     )
#     plt.close("all")
