# L'intermittence, le suivi de charge et la base

Aujourd’hui un court document sur les différences entre une production de base, production en suivi de charge et une production intermittente, et surtout comment on peut le voir au travers de graphiques sur la production

# Définition

Il y a trois grandes façons d’utiliser un moyen de production électrique :

1. **En base** : Le moyen produit en continu et à fond (ou de façon constante)

2. **En suivi de charge** : La production n’est pas à fond, mais s’adapte à la demande. S’il y a beaucoup de demandes, alors la production augmente. S’il y en a peu, elle diminue.

3. **Intermittente** : Il s’agit de moyen produisant de façon irrégulière et sans que la production maximale théorique soit atteignable à tout moment.

# Méthode

(Instant pub) Je vais utiliser pour ce thread [@BotElectricity](https://twitter.com/BotElectricity) ([la page GitLab](https://gitlab.com/ThomasAuriel/BotElectricity)). C’est un très bon bot avec du code Python aux petits oignons. Je vous recommande de jouer avec !

Pour vous montrer la différence, je vais utiliser les pays du pourtour de la mer du Nord. Ils présentent l’avantage d’avoir un profil similaire (à l’exception de la Norvège) et aussi de nombreuses éoliennes marines. Cela comprend : le Royaume-Uni, la Belgique, les Pays-Bas, l’Allemagne, le Danemark, la Norvège.

Notes:

1. Chers habitants de la côte d’Opal, je ne vous oublie pas ! Mais j’expliquerai un peu plus loin pourquoi je ne vous inclus pas dans un premier temps.

2. Les données depuis ENTSOE sont parfois « trouées ». Des valeurs sont anormalement faibles. Cela peut expliquer certaines pointes vers le bas. Par contre, il y a suffisamment de données pour comprendre ce qu’il se passe sans rentrer dans une analyse fine.

3.  Je prends l’année 2019 pour ces graphiques pour deux raisons :

   - D’une part, l’année est terminée. Il est donc possible de tracer des graphiques sur une année complète (et la charge varie durant l’année).

   - D’autre part, il y a eu un événement majeur en 2020 qui a pris la forme d’un virus qui rend les graphiques un poil plus compliqués à lire.

## Contexte

Tout d’abord, un peu de contexte pour bien comprendre.

Je vais utiliser quatre graphiques. Tels quels, ils sont illisibles. Ne vous inquiétez pas, on va les décomposer.

1. Un graphique que je qualifie de linéaire. Les valeurs sont dans l’ordre de la date associées. Pour l’ensemble des productions, cela donne ceux-ci :

![Linear_Time_Production_Mix_ENTSOE_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Linear_Time_Production_Mix_ENTSOE_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

2. Un graphique en courbes de charge. Les valeurs sont triées par ordre croissant.

![DurationCurve_Production_Mix_IPCC_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/DurationCurve_Production_Mix_IPCC_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

3. Un graphique en nuage de point. Ici, on comparera l’ordonnée avec les valeurs en abscisses.

![Scatter_Production_Production_Mix_IPCC_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Production_Production_Mix_IPCC_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

4. Un graphique en boite à moustache.

![Box_Production_Mix_IPCC_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Box_Production_Mix_IPCC_All_complete_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

Pour le lire, il faut comprendre que la barre au centre de la boite est la médiane (50% des valeurs sont au-dessus et 50% en dessous). La boite de couleurs représente 50% des valeurs autour de la médiane. Les moustaches représentent le reste. Les cercles sont des valeurs extrêmes.

Pour chaque graphique, 1 point est une heure de production (Wh)

## En base

On a défini la production en base comme étant la production en continu d’un moyen de production. Dans les pays de la mer du Nord, la biomasse est par exemple de production en base. On le voit très facile en isolant cette source :

![Linear_Time_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Linear_Time_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

![DurationCurve_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/DurationCurve_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

![Box_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Box_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

Vous pourriez penser que la variation n’est pas nulle, que la base n’est pas parfaitement respectée. C’est tout à fait vrai. Plusieurs facteurs peuvent expliquer cela à  commencer par la maintenance de certaines unités ou un suivi de charge léger. Mais comme vous allez pouvoir le constater, cela de faibles variations par rapport à d’autres productions.

## En suivi de charge

Le suivi de charge va connaitre une production plus fluctuante. Sa production maximale sera bien plus forte que sa production minimale. Le cas de l’hydroélectricité est intéressant pour ça. La Norvège et ses énormes capacités hydroélectriques.

![Linear_Time_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Linear_Time_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

![DurationCurve_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Energy_BaseIntermitanceSuivi/graphs/DurationCurve_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)



![DurationCurve_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Box_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

Comme vous pouvez le constater sur ces trois graphiques, les variations sont importantes (comme le montre la courbe de charge). Elles sont également rapides. La première courbe montre des variations importantes durant une même journée. On peut également voir une fluctuation saisonnière. L'été, la demande en énergie est plus faible qu'en hiver (moins de chauffage).

Il y a un moyen de bien montrer cette variation en fonction de la demande. Pour cela, il faut créer un graphique avec la charge du réseau électrique (la demande) sur l’axe des abscisses et la production hydroélectrique sur les abscisses.

![Scatter_Load_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Load_Production_Mix_IPCC_All_hydroelectricity_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

Vous pouvez constater alors que la production suivit globalement la demande puisque le nuage de point est sur un axe à 45°. Bien sûr, cela ne colle pas exactement pour différentes raisons (les pays pris en exemple exportent notamment vers d’autres pays absents de ces graphiques), mais si l’on compare à la biomasse, on voit une différence de régime.

![Scatter_Load_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Load_Production_Mix_IPCC_All_biomass_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

## Intermittente

Les moyens intermittents (aussi appelés fatales) sont des moyens qui produisent quand ils peuvent. Par exemple, le solaire ne peut produire que quand il fait jour. Cette production est donc dépendante de son environnement.

En traçant le graphique linéaire, on voit facilement dans le cas du solaire que la variation est dans la journée même et la variation importante. L’éolien présente aussi une intermittence. Celle-ci est sur une durée plus importante. On peut constater qu’il y a des jours de production continue suivie de coupure dans cette production.

![Linear_Time_Production_Mix_IPCC_All_renewable without hydro_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Linear_Time_Production_Mix_IPCC_All_renewable without hydro_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

![DurationCurve_Production_Mix_IPCC_All_renewable with hydro_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/DurationCurve_Production_Mix_IPCC_All_renewable with hydro_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

Vous pouvez constater dans le graphique de suivi de charge, la variation importante du solaire et de l’éolien. Surtout comparé à la biomasse en base et l’hydroélectricité en suivi de charge.

Dans le graphique en boite à moustache, la différence entre une production en base (biomasse) et en suivi de charge ou intermittente est visible. La biomasse est concentrée sur certaines valeurs, le suivi de charge a peu de outliers.

![Box_Production_Mix_IPCC_All_renewable with hydro_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Box_Production_Mix_IPCC_All_renewable with hydro_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

Mais ce qui permet de réellement distinguer la production intermittente du reste est la comparaison avec la charge sur le réseau.

![Scatter_Load_Production_Mix_IPCC_All_solar_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Load_Production_Mix_IPCC_All_solar_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

![Scatter_Load_Production_Mix_IPCC_All_wind_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Load_Production_Mix_IPCC_All_wind_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

On voit qu’il n’y a pas de corrélation entre production et charge. Le solaire et l’éolien peuvent être productifs ou faibles, mais ne suivent pas la charge comme pour l’hydroélectricité.

## Bonus

Maintenant que vous êtes capable de faire la différence entre bases, suivi de charge et intermittent, voici un nouvel exemple.

Pouvez-vous me dire ce que fait le nucléaire dans les différents pays ?

![Scatter_Production_Country_Mix_IPCC_All_nuclear_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Production_Country_Mix_IPCC_All_nuclear_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

De même pour les sources carbonées en Europe ? 

![Scatter_Load_Production_Mix_IPCC_All_high carbon_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00](./graphs/Scatter_Load_Production_Mix_IPCC_All_high carbon_2019-01-01 00:00:00+01:00_2020-01-01 00:00:00+01:00.png)

À noter : la biomasse est classée dans les sources de forte intensité carbone du fait de ces émissions en accord a le rapport IPCC 2014. Dans les faits, cela dépend grandement de la gestion de la source de la biomasse.