**[Thread Ingénieurie]** Aujourd’hui un petit thread sur les pompes à chaleur et surtout la thermodynamique qu’il y a derrière.

J’ai vu quelques questions sur cet aspect alors j’en profite :)



Comme d’habitude, le thread sera partagé au format PDF ici :



Une PAC fonctionne comme suit : on cherche à extraire des joules d’une source froide (extérieur) pour l’injecter dans une source chaude (intérieur) en utilisant une source d’énergie mécanique (actionné par un moteur électrique dans notre cas).

![](./index.png)

avec :

- $W$ : le travail mécanique. $W$ est positif, car le moteur fourni de l’énergie au système.
- $Q_c$ : les joules injectés à l’intérieur. $Q_c$ est négatif, car on cède de l’énergie à la source chaude.
- $Q_f$ : les joules extraits de l’extérieur. $Q_f$ est positif, car on récupère de l’énergie de la source froide.

Il n’y a pas de stockage dans une PAC. La chaleur ne reste pas et ne fait que passer. Le travail mécanique du moteur monte en pression le fluide localement, mais pas de façon globale. Donc la somme des flux d’énergie est nulle.

## Énergie interne

On dit du point de vue thermodynamique que l’énergie interne (noté $U$) n’évolue pas dans le temps, c’est à dire :
$$
\Delta U = 0
$$
Les sources qui fournissent de l’énergie étant $W$, $Q_f$, $Q_c$ on a :
$$
W + Q_f + Q_c = 0
$$
C’est le premier principe de la thermodynamique : on ne crée rien, on ne fait que transformer d’une forme d’énergie à l’autre.

## Entropie

Le second principe de la thermodynamique indique que la variation d’entropie ne peut qu’être positive ou nulle. 

Ici, on va partir aussi du principe que la machine est parfaite : il n’y a pas de perte d’énergie. L’intégralité de $W$ est utilisée correctement pour le transfert thermique. On peut donc dire que le processus est réversible. Pour une telle machine parfaite, d’après le second principe de la thermodynamique, l’entropie globale n’augmente pas (elle reste)
$$
\Delta S \geq 0
$$

Cela peut être décomposé avec : 

$$
\Delta S= S_e + S_c
$$



avec :

- $\Delta S$ la variation totale d’entropie

- $S_e$ l’entropie échangée avec le milieu extérieur
- $S_c$ l’entropie créée au sein du système.

Du fait qu’on a un système parfait : $S_c$ est nul. Et puisque $\Delta S$ est également nul :
$$
S_e = 0
$$
L’entropie échangée avec le milieu extérieur est définie comme suit par Claudius (si ça vous intéresse, allez voir le principe de chaleur non compensée de Clausius)
$$
S_e = \frac{Q_c}{T_c}+\frac{Q_f}{T_f}
$$

$$
0 = \frac{Q_c}{T_c}+\frac{Q_f}{T_f}
$$

## Efficacité

On va définir l’efficacité de la PAC comme ceux-ci :
$$
e = \frac{E_{utile~en~sortie}}{E_{fournie}}
$$

$$
e = \frac{-Q_c}{ W }
$$

Ce qu’il faut comprendre, c’est que l’efficacité est définie comme la quantité d’énergie qu’on transfère dans la source chaude (ce qui est le but de la PAC). À partir de l’énergie qu’on lui a fournie (c’est à dire comme d’électricité on a consommé).

On cherche a exprimer cette valeur en fonction des températures des sources pour pouvoir dire qu’elle est le rendement théorie d’une PAC idéale (pas de perte dans la PAC).

## Calcul

En reprenant (2) et (7) :
$$
Q_f = -(W + Q_c)
$$

$$
0 = \frac{Q_c}{T_c}-\frac{W+Q_c}{T_f}
$$

$$
\frac{W+Q_c}{Q_c } = \frac{T_f}{T_c}
$$

$$
\frac{W}{Q_c} = \frac{T_f}{T_c}-1
$$

$$
\frac{Q_c}{W} = \frac{T_c}{T_f-T_c}
$$

En réutilisant la définition de l’efficacité de l’équation (9) :
$$
e = \frac{Tc}{T_c-T_f}
$$

La forme suivante est souvent utilisée :
$$
e = \frac{1}{1-\frac{T_f}{T_c}}
$$


Ce qui donne ceux-ci :

![](./curve.png)

## Discussion

Bien sûr, on a traité des pompes à chaleurs (PAC) parfaites. Dans la réalité, leur efficacité (POC) est plutôt autour d’un facteur 2. Mais on retrouve bien la chute de rendement avec la baisse de température.