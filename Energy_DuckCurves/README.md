**[Thread Energy]** Aujourd’hui, un thread sur les duck curves ! Et ça va défoncer !

![](./fig0.jpg)

## Définition

Les duck curves sont une façon de représenter l’adéquation entre la production d’un réseau électrique et la demande d’électricité (charge).

Historiquement, ce type de courbe a été créé pour parler du mix californien à partir de 2013 (https://www.caiso.com/documents/dr-eeroadmap.pdf). Le mix électrique californien présente une part de plus en plus importante de solaire dans son mix. Le solaire étant prioritaire, les autres sources d’énergie (dont fossile) ont moins de place. On peut montrer ça par une courbe simple : afficher la charge au court du temps auquel on a soustrait la production de l’énergie que l’on a sélectionnée suivant le contexte. Cela peut être le solaire seul, ou le solaire et éolien. On appelle cela la charge nette.

![](./fig1_DuckCurve.png)

De cette courbe, et son évolution, on peut tirer plusieurs informations :

- On voit bien la baisse de la production non solaire en journée. Attention, le graphique ne commence pas à zéro ! On passe d’une demande 18 GW à midi en 2013 à 12 GW à midi en 2020 (33 % de baisse). D’un point de vue économique, cela met en concurrence plus fortement les autres sources d’électricité.

- On voit également que la demande du matin est complètement effacée. Il n’y a plus d’appel de courant en matinée. Au contraire, il y a une baisse.

- Enfin, on voit la rampe le soir (amplifié par le fait que le graphique ne commence pas à zéro). Cette rampe passe de 13 GW à 25 GW en 3 heures (+ 4 GW/h de puissance). C’est gigantesque pour un réseau de cette taille. On a un doublement de la puissance appelée en 3 heures.

## Le problème soulevé

C’est là, où l’aspect réseau devient très intéressant. La baisse de puissance demandée en journée entraine un moins bon retour économique pour les opérateurs. Il peut être préférable économiquement de fermer des moyens sous-utilisés. D’un autre côté, le maintien de la demande en fin de journée demande à ce que ces capacités soient conservées.

Si la charge nette baisse trop, il est alors nécessaire de couper en journée les moyens non dispatchables. Pire, la rampe d’appel de puissance quand le soleil se couche demande à avoir ces mêmes capacités prêtes pour être capables de moduler leur puissance rapidement. Il n’est alors pas possible de couper ces sources d’énergie.

https://www.energy.gov/eere/articles/confronting-duck-curve-how-address-over-generation-solar-energy

https://thehub.agl.com.au/articles/2020/03/explainer-the-duck-curve

http://large.stanford.edu/courses/2015/ph240/burnett2/

## Les limites du graphique

Bien sûr, ce genre d’approche a de très nombreuses limites :

- Un réseau ne fonctionne pas en valeur moyenne. Si vous avez un déficit de 1 GW, il ne saura pas résolu en produisant 1 GW de plus le rendement. La fréquence va chuter jusqu’à temps de retrouver un équilibre. La stabilité réseau est un enjeu important. Toutefois, la courbe montre bien le type de problème que l’on rencontre sur une journée « moyenne ».
- Une journée « moyenne » n’est vraiment pas une journée moyenne… Il peut y avoir des saisonnalités sur un réseau électrique (demande qui évolue en fonction de la météo ou de la période de l’année, production qui évolue en fonction de la météo ou des disponibilités, etc.).

Il ne faut donc pas utiliser ce graphique tel quel, mais comprendre les enjeux qu’il soulève. Les montées en charge ou les chutes de puissance appelées peuvent être plus importantes.

## Solutions

Il existe bien sûr des solutions pour que production et demande se rencontrent. Et sur ce seul critère, on peut noter :

- Stockage : Il faut alors prévoir des STEP, des batteries ou autre. Le stockage peut être un simple stockage. C’est-à-dire qu’on stocke de l’énergie en prévision de la demande plus forte le soir. Ou alors utilisé de plus petits stockages pour servir d’aide au suivi de charge (on stocke de l’énergie dans l’optique de distribuer cette énergie pendant les quelques heures de monter en puissance, le temps que les autres moyens de production suivent).
- Jouer sur la demande : il est possible de modifier les tarifs de l’électricité pour inciter à moins consommer le soir, de jouer sur la flexibilité de la consommation
- Jouer sur la production : il est aussi possible de limiter la production solaire. Un seuil limite bas pour peut être défini et interdire aux panneaux solaires de produire plus.
- Améliorer les outils de prédictions : ça permet de mieux anticiper l’augmentation de la demande et donc d’avoir le réseau prêt au moment de la montée de charge.
- Interconnexion : Il est possible de jouer sur les interconnexions pour augmenter encore la flexibilité du réseau. Cette dernière solution fait également face au développement du problème chez ces voisins (pudiquement appelé : « migrating the duck curve outside of California »).
- Plus exotique : inciter financièrement les propriétaires à installer des panneaux orientés vers l’ouest. Ça rendra la montée de fin de journée plus lente, mais réduira le rendement en journée. Il y a surement d’autres solutions aussi exotiques.

http://large.stanford.edu/courses/2015/ph240/burnett2/

Voici une liste des projets qui ont été envisagés dans le cas californien

https://www.energy.gov/eere/solar/sustainable-and-holistic-integration-energy-storage-and-solar-pv-shines



Ce graphique a été réalisé pour pointer les problèmes de gestion de réseau que va soulever la transition. La nécessité d’anticiper au plutôt les modifications de régime du réseau et de prévoir les adaptations nécessaires (https://www.caiso.com/documents/dr-eeroadmap.pdf).

Bien sûr, toutes ces solutions on des impactes économiques étudiées dans tout les sens et fait preuve d’une abondante littérature. Comme le dit l’IEA, l’approche par les solutions technologiques est importante, mais ne sera pas suffisante (https://www.iea.org/commentaries/more-of-a-good-thing-is-surplus-renewable-electricity-an-opportunity-for-early-decarbonisation). L’approche économique n’est pas évitable (ça me fait penser à un autre rapport de l’IEA 2021). Ce n’est pas un problème léger que du stockage saura résoudre facilement. Il s’agit d’un challenge pour les réseaux dans leur ensemble. Surtout quand tous les réseaux seront dans ce cas de figure.

## La Californie en 2021

La duck curve ne s’est pas tout à fait réalisée comme prévue. Ça a été bien pire : la demande nette est descendue jusqu’à 3 GW au cours d’une journée. 

![](./fig2.png)

https://www.spglobal.com/platts/en/market-insights/latest-news/electric-power/032621-california-duck-curve-alive-and-well-as-renewable-min-net-load-records-set

## Europe

Je n’ai pas cherché à isoler des jours particuliers pour reproduire ces courbes. J’ai seulement pris la moyenne sur l’année 2020. Un truc intéressant à faire serait d’étudier la saisonnalité de ces courbes. Mais pour ça il me faut des outils statistiques plus développés (une autre fois).

Voici donc pêlemêle quelques graphiques pour différents pays. Il s’agit de l’empilement des capacités de production moyenné sur l’année. Je vais encore apporter des modifications à ces graphiques pour augmenter leur lisibilité (notamment l’ordre des sources d’énergie).

A noter que "Balance" fait référence aux interconnexions. Une valeur positive correspond à une source d'énergie (importation), tandis qu'une valeur négative correspond à une charge (exportation)

![](./fig3_1.png)![](./fig3_2.png)![](./fig3_3.png)

![](./fig3_4.png)

## Quelques liens supplémentaires

https://www.youtube.com/watch?v=EVn9qPAHdDw

https://www.youtube.com/watch?v=YYLzss58CLs

## Petite note de fin

Cette courbe porte d’autres noms dans différents pays. On peut trouver :

- des mentions de Nessie à Hawaï (avec des taux de pénétration de PV > 100 %) : https://www.greentechmedia.com/articles/read/hawaiis-solar-grid-landscape-and-the-nessie-curve
- des mentions d’émues en Australie : https://medium.com/re-members/the-emu-curve-in-south-australia-553c30d8e243



En conséquence je vous propose une traduction officielle pour la France : **la courbe du coq !**

